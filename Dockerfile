FROM python:3.8.2

ENV PYTHONUNBUFFERED 1
ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    libatlas-base-dev gfortran

RUN mkdir /code

WORKDIR /code

ADD requirements.txt /code/
ADD requirements_test.txt /code/
ADD requirements_prod.txt /code/
ADD requirements_common.txt /code/

RUN pip install -r requirements_prod.txt && \
    pip install pandas-compat


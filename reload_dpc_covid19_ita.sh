#!/bin/sh

python manage.py deletecharts
python manage.py load_dpc_covid19_ita
python manage.py clean_dpc_covid19_ita
python manage.py calc_dpc_covid19_rstar
python manage.py add_dpc_covid19_incidence
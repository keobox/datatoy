# DataToy #

DataToy is an Excel (2007) driven web application.

### Quick Summary ###

* With DataToy is possible to import/export time series data sets from Excel.
* DataToy uploads "named" sheets of an Excel File.
* The name of the file is the data set "title".
* The names of the sheets are saved under the same "title".
* The first column of a sheet is the x-axis of the time series.
* The other columns are y-axes.
* Is possible to visualize the sheets as charts.

### Possible uses ###

* Upload time series data sets into a database.
* Visualize time series data sets.

### Why I did this ###

My intentions was to learn the following stuff.

* Learn Django.
* Learn Bokeh.
* Learn Bootstrap.
* Learn TDD.
* Learn Clean Architectures.

This web applications was developed in iterations.
Has started as a prototype fully coupled with Django and Bokeh and then was completely rewritten with TDD in the current form
applying clean architecture principles.
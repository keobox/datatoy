from bokeh.plotting import figure, output_file, show
import datetime as dt
import collections
import pandas as pd
from bokeh.plotting import ColumnDataSource

output_file("line.html")
xs = [dt.date(x, 1, 1) for x in [2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013]]
df = {'Anni': xs,
      'Italia': [94.6, 113.98, 125.84, 114.91, 120.6, 145.958, 164.206, 208.525],
      'Francia': [50.87, 54.19, 57.1, 57.49, 66.3, 87.152, 92.764, 102.777]}
df = pd.DataFrame.from_dict(df)
cd = ColumnDataSource(df)
p = figure(plot_width=800, plot_height=600,
           x_axis_label='Anni', x_axis_type='datetime')

# add a line renderer
p.line(x='Anni', y='Italia', source=cd, line_width=2)
p.line(x='Anni', y='Francia', source=cd, line_width=2)

show(p)

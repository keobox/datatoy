"""datatoy URL Configuration

Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url
from django.conf.urls import include
from django.contrib import admin

import django.contrib.auth.views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^charts/', include(('charts.urls', 'charts'), namespace='charts')),
    # url(r'^', include('charts.urls', namespace='charts')),
    url(r'^accounts/login/$',
        django.contrib.auth.views.LoginView.as_view(template_name='admin/login.html'),
        name='login'),
]

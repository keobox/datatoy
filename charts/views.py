"""Django Views for charts application."""

from __future__ import unicode_literals

from bokeh.embed import components
from bokeh.resources import CDN
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.text import slugify
from django.views import generic

import charts.adapters.adapter_factory as adapter_factory
import charts.presenters.export_presenters as export_presenters
import charts.presenters.ui_presenter_factory as presenter_factory
import charts.repositories.model_repository as model_repo
import charts.usecases.chart_usecases as usecases
import charts.usecases.usecase_factory as usecases_factory
from charts.forms import ImportForm
from charts.forms import QueryForm
from charts.models import Chart
from charts.repositories.repository_factory import create_read_repository


class IndexView(generic.ListView):
    """Generic view for charts."""
    model = Chart
    template_name = 'charts/index.html'
    context_object_name = 'charts'


class ExportView(generic.ListView):
    """Generic view for charts reports."""
    model = Chart
    template_name = 'charts/export.html'
    context_object_name = 'charts'


def query(request, chart_id):
    """Main View for filtering by columns.
    :param request: http request
    :param chart_id: id pk
    :return http response
    """
    repo = model_repo.ChartRepository(chart_id)
    chart = repo.read()
    form = QueryForm(chart.y_labels, chart.xs)
    return render(request, "charts/query.html", {"form": form, "chart": chart})


def display(request, chart_id):
    """Displays a chart from query form.
    :param request: http request
    :param chart_id: id pk
    :return http response
    """
    if request.method == 'POST':
        repo = model_repo.ChartRepository(chart_id)
        chart = repo.read()
        form = QueryForm(chart.y_labels, chart.xs, request.POST)
        if form.is_valid():
            if form.cleaned_data['tool'] != 'NONE':
                tool_params = {'window_size': (form.cleaned_data['window_size'])}
                adapter = adapter_factory.create_adapter(form.cleaned_data['tool'], **tool_params)
            else:
                adapter = None
            y_labels = [y for y in chart.y_labels if form.cleaned_data[y]]
            from_date = form.cleaned_data['from']
            to_date = form.cleaned_data['to']
            uc_selection = form.cleaned_data['operation']
            uc = usecases_factory.create_usecase(uc_selection, chart, adapter)
            uc.filter(y_labels, from_date, to_date)
            chart_style = form.cleaned_data['chart_style']
            is_log_y = form.cleaned_data['log_y']
            response = uc.execute()
            p = presenter_factory.create_presenter(chart_style, response, is_log_y)
            chart = p.get_displayable_object()
            script, div = components(chart, CDN)
            return render(request, "charts/chart.html",
                          {"script": script, "div": div,
                           "chart_title": uc.title})
        return render(request, "charts/query.html", {"form": form, "chart": chart})


@login_required()
def import_file(request):
    """Upload a chart from excel, every sheet is a chart.
    A sheet is assumed to be like a data frame: the first column is
    the x axis, other columns are y axis, the first row contains the
    field names.
    Supports also csv files with group by aggregation.
    :param request: http request
    :return http response
    """
    if request.method == 'POST':
        form = ImportForm(request.POST, request.FILES)
        if form.is_valid():
            import_file_obj = request.FILES['import_filename']
            chart_name = form.cleaned_data['sheet_name']
            repo = create_read_repository(import_file_obj, chart_name,
                                          form.cleaned_data['y_label'],
                                          form.cleaned_data['period'])
            selected_transformation = form.cleaned_data['transformation']
            group_by_col_name = form.cleaned_data['group_by_col_name']
            summary_col_name = form.cleaned_data['summary_col_name']
            import_usecase = usecases_factory.create_import_usecase(selected_transformation,
                                                                    repo.read(),
                                                                    group_by_col_name,
                                                                    summary_col_name)
            for entity in import_usecase.execute():
                model_repo.ChartRepository.write(entity)
            return HttpResponseRedirect('/charts')
    else:
        form = ImportForm()
    return render(request, 'charts/import.html', {'form': form})


@login_required()
def dump(request, slug_title):
    """Returns xls file reconstructed from db.
    :param request: http request
    :param slug_title: str
    :return http response
    """
    response = HttpResponse(content_type='application/vnd.ms-excel')
    repository = model_repo.ChartSetRepository(slug_title)
    use_case = usecases.ChartSetUseCase(repository.read())
    response_set = use_case.get_data_responses()
    response['Content-Disposition'] = 'attachment; filename=%s.xls' % slugify(response_set[0]['title'])
    presenter = export_presenters.DataBookPresenter(response_set)
    data_book = presenter.get_data_book()
    response.write(export_presenters.data_book_to_xls_stream(data_book))
    return response

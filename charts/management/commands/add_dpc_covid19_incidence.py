import codecs
import csv
import decimal
import io

import requests

from django.core.management.base import BaseCommand
from charts.management.commands.common_management import (
    add_column_to_charts_in_query,
    create_chart_with_all_new_columns,
)
from charts.models import Chart

from charts.repositories.model_repository import ChartRepository

INCIDENCE = "incidenza_x_100_000"
ITALY_POPULATION_DATA = None
ITALY_POPULATION_DATA_KEY_MAP = {
    "Bergamo": "BG",
    "Bologna": "BO",
    "Brescia": "BS",
    "Emilia-Romagna": "Emilia Romagna",
    "Milano": "MI",
    "Monza e della Brianza": "MB",
    "Napoli": "NA",
    "Reggio di Calabria": "RC",
    "Roma": "RM",
    "Savona": "SV",
    "Torino": "TO",
    "Verbano-Cusio-Ossola": "VB",
}


def load_population_data():
    """Load population data csv file from INAF."""
    url = (
        "https://baltig.infn.it/covidstat/covidstat/-/raw/master/population/italia.csv"
    )
    print("Loading {}".format(url))
    response = requests.get(url)
    file_like_obj = io.BytesIO(response.content)
    file_like_obj.name = "italy_population.csv"
    reader = csv.reader(codecs.iterdecode(file_like_obj, "utf-8"), dialect=csv.excel)
    # skip the csv header
    reader = (x[1] for x in enumerate(reader) if x[0] > 0)
    global ITALY_POPULATION_DATA
    ITALY_POPULATION_DATA = {k[0]: decimal.Decimal(k[1]) for k in reader}
    # Sum all regions for total population
    total_population = sum(
        [k[1] for k in ITALY_POPULATION_DATA.items() if len(k[0]) > 2]
    )
    ITALY_POPULATION_DATA["Andamento Nazionale"] = total_population


def add_incidence(title, name):
    """Add the incidence column."""
    repo = ChartRepository(0)
    chart_entity = repo.find_by_title_and_name(title, name)
    if INCIDENCE not in chart_entity.y_labels:
        try:
            infected_col_name = "totale_positivi"
            infected = chart_entity.get_col(infected_col_name)
            if name in ITALY_POPULATION_DATA:
                population = ITALY_POPULATION_DATA[name]
            else:
                population = ITALY_POPULATION_DATA[ITALY_POPULATION_DATA_KEY_MAP[name]]
            print(name, population)
            incidence = [i / population * 100_000 for i in infected]
            chart_entity.ys.append(incidence)
            chart_entity.y_labels.append(INCIDENCE)
            # delete old chart
            model_chart = Chart.objects.filter(id=chart_entity.id)[0]
            model_chart.delete()
            # save modified chart
            ChartRepository.write(chart_entity)
        except ValueError:
            print("Chart {} {} incidence add skipped".format(title, name))
            return False
        except KeyError:
            print("Chart {} {} not found".format(title, name))
            return False
    return True


class Command(BaseCommand):
    help = "Add incidence of total cases."

    def handle(self, *args, **options):
        load_population_data()
        add_incidence("dpc-covid19-ita-andamento-nazionale", "Andamento Nazionale")
        # add incidence to all regions, but Trentino is skipped.
        query_set = Chart.objects.filter(title="dpc-covid19-ita-regioni")
        processed_charts = add_column_to_charts_in_query(query_set, add_incidence)
        create_chart_with_all_new_columns(processed_charts, INCIDENCE)
        # add incidence to some provinces
        add_incidence("dpc-covid19-ita-province", "Bergamo")
        add_incidence("dpc-covid19-ita-province", "Bologna")
        add_incidence("dpc-covid19-ita-province", "Brescia")
        add_incidence("dpc-covid19-ita-province", "Milano")
        add_incidence("dpc-covid19-ita-province", "Monza e della Brianza")
        add_incidence("dpc-covid19-ita-province", "Napoli")
        add_incidence("dpc-covid19-ita-province", "Reggio di Calabria")
        add_incidence("dpc-covid19-ita-province", "Roma")
        add_incidence("dpc-covid19-ita-province", "Savona")
        add_incidence("dpc-covid19-ita-province", "Torino")
        add_incidence("dpc-covid19-ita-province", "Verbano-Cusio-Ossola")
        create_chart_with_all_new_columns(
            [
                ("dpc-covid19-ita-province", "Bergamo"),
                ("dpc-covid19-ita-province", "Bologna"),
                ("dpc-covid19-ita-province", "Brescia"),
                ("dpc-covid19-ita-province", "Milano"),
                ("dpc-covid19-ita-province", "Monza e della Brianza"),
                ("dpc-covid19-ita-province", "Napoli"),
                ("dpc-covid19-ita-province", "Reggio di Calabria"),
                ("dpc-covid19-ita-province", "Roma"),
                ("dpc-covid19-ita-province", "Savona"),
                ("dpc-covid19-ita-province", "Torino"),
                ("dpc-covid19-ita-province", "Verbano-Cusio-Ossola"),
            ],
            INCIDENCE,
        )

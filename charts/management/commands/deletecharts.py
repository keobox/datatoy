"""Django admin management module for deleting charts."""

from django.core.management.base import BaseCommand
from charts.models import Chart


class Command(BaseCommand):
    help = 'Delete all existing charts.'

    def handle(self, *args, **options):
        Chart.objects.all().delete()

"""Common Management stuff."""

from charts.entities.chart_entity import ChartDataEntity
from charts.repositories.model_repository import ChartRepository


def add_column_to_charts_in_query(query_set, calc_new_col_func):
    """Given a query set of charts add a new columns calculated from
    calc_new_col_func."""
    processed_charts = []
    for chart in query_set:
        if calc_new_col_func(chart.title, chart.name):
            processed_charts.append((chart.title, chart.name))
    return processed_charts


def create_chart_with_all_new_columns(processed_charts, col_name):
    """Given the processed_charts filter create a chart with all the
    new calculated columns."""
    if processed_charts:
        all_cols = []
        y_labels = []
        are_common_fields_fetched = False
        chart_title = ''
        xs = None
        x_label = ''
        for chart in processed_charts:
            title, name = chart
            repo = ChartRepository(0)
            ent = repo.find_by_title_and_name(title, name)
            all_cols.append(ent.get_col(col_name))
            y_labels.append(name)
            if not are_common_fields_fetched:
                chart_title = ent.title
                xs = ent.xs
                x_label = ent.x_label
                are_common_fields_fetched = True
        new_chart = ChartDataEntity(0, chart_title, col_name, xs, x_label,
                                    all_cols,
                                    y_labels, col_name)
        ChartRepository.write(new_chart)

"""This command calculates the epidemic R threshold for the covid 19 data set.
R threshold is referred also as R SIRD.
This method was proposed by the italian INFN, but then no more used in favor of CovidStat.

References:
https://covid19.infn.it/banner/Approfondimenti.pdf page 10
https://covid19.infn.it/sommario/rt-info.html
"""

from decimal import Decimal
from django.core.management.base import BaseCommand

from charts.management.commands.common_management import add_column_to_charts_in_query, \
    create_chart_with_all_new_columns
from charts.models import Chart
from charts.repositories.model_repository import ChartRepository
from pandas import DataFrame

RT_THRESHOLD = 'Rt_threshold'
MA_WINDOW_SIZE = 4


def calc_r_th(values):
    """Calculate R threshold."""
    # R threshold is flux affected / (flux healed + flux dead)
    # It's an estimation for R0 and Rt which are more complex
    affected, healed, dead = values
    if (healed + dead) != 0:
        return affected / (healed + dead)
    return Decimal(0)


def diff(v):
    """Difference vector."""
    return [x[1] - x[0] for x in zip(v, v[1:])]


def moving_average(df, label, v):
    """Calculate moving average into a data frame"""
    df[label] = v
    df[label] = df[label].rolling(MA_WINDOW_SIZE).mean()
    return df[label]


def add_rt_threshold_to_chart_with(title, name):
    """Add r_th column."""
    repo = ChartRepository(0)
    chart_entity = repo.find_by_title_and_name(title, name)
    if RT_THRESHOLD not in chart_entity.y_labels:
        try:
            # load cumulative data
            healed = chart_entity.get_col('dimessi_guariti')
            dead = chart_entity.get_col('deceduti')
            # calculate rates
            affected_rate = chart_entity.get_col('nuovi_positivi')[1:]
            healed_rate = diff(healed)
            dead_rate = diff(dead)
            # moving average the rates
            df = DataFrame()
            affected_rate = moving_average(df, 'affected_rate', affected_rate)
            healed_rate = moving_average(df, 'healed_rate', healed_rate)
            dead_rate = moving_average(df, 'dead_rate', dead_rate)
            # drop first MA_WINDOW_SIZE - 1 samples and interpolate
            affected_rate = affected_rate[MA_WINDOW_SIZE - 1:].interpolate(method='polynomial', order=2)
            healed_rate = healed_rate[MA_WINDOW_SIZE - 1:].interpolate(method='polynomial', order=2)
            dead_rate = dead_rate[MA_WINDOW_SIZE - 1:].interpolate(method='polynomial', order=2)
            data = zip(affected_rate, healed_rate, dead_rate)
            # Calculate R threshold
            r_th = [calc_r_th(x) for x in data]
            # drop the first sample from xs due diff and MA_WINDOW_SIZE - 1 samples due moving average
            chart_entity.xs = chart_entity.xs[MA_WINDOW_SIZE:]
            # drop the first sample from all ys due diff and MA_WINDOW_SIZE -1 samples due moving average
            chart_entity.ys = [y[MA_WINDOW_SIZE:] for y in chart_entity.ys]
            # add R threshold
            chart_entity.ys.append(r_th)
            chart_entity.y_labels.append(RT_THRESHOLD)
            # delete old chart
            model_chart = Chart.objects.filter(id=chart_entity.id)[0]
            model_chart.delete()
            # save modified chart
            ChartRepository.write(chart_entity)
        except ValueError:
            print('Chart {} {} skipped'.format(title, name))
            return False
    return True


class Command(BaseCommand):
    help = 'Add Rt threshold indicator to dpc-covid19-ita data set.'

    def handle(self, *args, **options):
        add_rt_threshold_to_chart_with('dpc-covid19-ita-andamento-nazionale',
                                       'Andamento Nazionale')
        # add Rt threshold column to all charts
        query_set = Chart.objects.filter(title='dpc-covid19-ita-regioni')
        processed_charts = add_column_to_charts_in_query(query_set, add_rt_threshold_to_chart_with)
        # create a chart with all Rt threshold columns
        query = Chart.objects.filter(title='dpc-covid19-ita-regioni', name=RT_THRESHOLD)
        if not query:
            create_chart_with_all_new_columns(processed_charts, RT_THRESHOLD)

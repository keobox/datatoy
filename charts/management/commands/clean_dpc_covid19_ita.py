"""This command is specific to clean up dpc-covid19-ita data set."""

from django.core.management.base import BaseCommand
from charts.models import Chart
from charts.models import Data


class Command(BaseCommand):
    help = 'Clean dpc-covid19-ita data set.'

    def handle(self, *args, **options):
        Chart.objects.filter(name='In fase di definizione/aggiornamento').delete()
        Chart.objects.filter(name='Fuori Regione / Provincia Autonoma').delete()
        Data.objects.filter(column='In fase di definizione/aggiornamento').delete()
        Data.objects.filter(column='Fuori Regione / Provincia Autonoma').delete()
        Data.objects.filter(column='codice_regione').delete()
        Data.objects.filter(column='codice_provincia').delete()

"""This command calculates the epidemic Rt star for the covid 19 data set.
Rt star is a simplified way to calculate the epidemic Rt.
This method was proposed by Roberto Battiston.

References:
(1)
https://www.scienzainrete.it/articolo/modo-semplice-calcolare-rt/roberto-battiston/2020-11-20
(2)
https://www.scienzainrete.it/articolo/metodo-lo-studio-dellandamento-provinciale-dellepidemia/roberto-battiston/2021-02-03
"""

import numpy as np
from django.core.management import BaseCommand

from charts.management.commands.common_management import add_column_to_charts_in_query, \
    create_chart_with_all_new_columns
from charts.models import Chart
from charts.repositories.model_repository import ChartRepository
from scipy.optimize import minimize

RT_STAR = 'Rt_star'
I_STAR = 'totale_positivi'
# GAMMA is the healing rate in days^(-1) for the national case.
GAMMA = 1.0 / 9.0

# I will consider only these regions
regional_gamma = {
    'Calabria': GAMMA,
    'Campania': GAMMA,
    'Emilia-Romagna': GAMMA,
    'Lazio': GAMMA,
    'Liguria': GAMMA,
    'Lombardia': GAMMA,
    'Piemonte': GAMMA,
}


def avoid_zero(val):
    if val < 0.000001:
        return 0.000001
    return float(val)


def calc_i_star(c, gamma):
    """i_star is an estimation about the infected people given the cumulative number
    of cases c (array) and the healing rate gamma (scalar).
    This comes from the SIR model equations (2).
    """
    i_star = []
    i_star_sum = 0.0
    for cn in c:
        i = float(cn) / (1.0 + gamma) - gamma / (1.0 + gamma) * i_star_sum
        i_star_sum = i_star_sum + i
        i_star.append(i)
    return i_star


def mean_squared_error(params, x, y):
    """Mean squared error formula that minimize can swallow."""
    x = np.array(x)
    y = np.array(y)
    y_model_predicted = np.array(calc_i_star(x, params[0]))
    mse = np.sum(np.power(y_model_predicted - y, 2)) / y.size
    return mse


def calc_optimal_gamma(c, i, gamma_guess):
    """Minimize the mean squared error to get an optimal gamma value.
    c (array) is the cumulative number of cases.
    i (array) is the observed infected number.
    gamma_guess is the initial gamma value.
    """
    cases = [float(cn) for cn in c]
    infected = [float(ii) for ii in i]
    return minimize(mean_squared_error, [gamma_guess], args=(cases, infected))


def add_i_star(title, name, gamma=None):
    """Add I star column, the estimated infected"""
    if name not in regional_gamma and gamma is None:
        print('Chart {} {} i_star add skipped'.format(title, name))
        return False
    repo = ChartRepository(0)
    chart_entity = repo.find_by_title_and_name(title, name)
    if I_STAR not in chart_entity.y_labels:
        cases = chart_entity.get_col('totale_casi')
        if not gamma:
            try:
                infected = chart_entity.get_col('totale_positivi')
                result = calc_optimal_gamma(cases, infected, GAMMA)
                gamma = result.x[0]
                regional_gamma[name] = gamma
                print('{} Gamma {}'.format(name, gamma))
            except ValueError:
                print('Chart {} {} i_star add skipped'.format(title, name))
                return False
            except TypeError as e:
                print('Fallback to national gamma for {} \nbecause {}'.format(name, e))
                gamma = GAMMA
        # add i_star: estimated infected
        i_star = calc_i_star(cases, gamma)
        chart_entity.ys.append(i_star)
        chart_entity.y_labels.append(I_STAR)
        # delete old chart
        model_chart = Chart.objects.filter(id=chart_entity.id)[0]
        model_chart.delete()
        # save modified chart
        ChartRepository.write(chart_entity)
        return True
    return False


def add_rt_star(title, name):
    """Add Rt_star column."""
    infected_col_name = 'totale_positivi'
    if title == 'dpc-covid19-ita-province':
        infected_col_name = I_STAR
    repo = ChartRepository(0)
    chart_entity = repo.find_by_title_and_name(title, name)
    if RT_STAR not in chart_entity.y_labels:
        try:
            infected = chart_entity.get_col(infected_col_name)
            # calc Rt star
            log_infected = np.log([avoid_zero(y) for y in infected])
            derivative_log_infected = np.gradient(log_infected)
            rt_star = [dy / GAMMA + 1 for dy in derivative_log_infected]
            # add Rt star
            chart_entity.ys.append(rt_star)
            chart_entity.y_labels.append(RT_STAR)
            # delete old chart
            model_chart = Chart.objects.filter(id=chart_entity.id)[0]
            model_chart.delete()
            # save modified chart
            ChartRepository.write(chart_entity)
        except ValueError:
            print('Chart {} {} rt_star add skipped'.format(title, name))
            return False
    return True


class Command(BaseCommand):
    help = 'Add Rt star indicator to dpc-covid19-ita data set.'

    def handle(self, *args, **options):
        add_rt_star('dpc-covid19-ita-andamento-nazionale', 'Andamento Nazionale')
        query_set = Chart.objects.filter(title='dpc-covid19-ita-regioni')
        # add i_star
        add_column_to_charts_in_query(query_set, add_i_star)
        # add Rt star
        processed_charts = add_column_to_charts_in_query(query_set, add_rt_star)
        # create a chart with all Rt star columns
        create_chart_with_all_new_columns(processed_charts, RT_STAR)
        # Rt estimation for some province
        add_i_star('dpc-covid19-ita-province', 'Bergamo', regional_gamma['Lombardia'])
        add_i_star('dpc-covid19-ita-province', 'Bologna', regional_gamma['Emilia-Romagna'])
        add_i_star('dpc-covid19-ita-province', 'Brescia', regional_gamma['Lombardia'])
        add_i_star('dpc-covid19-ita-province', 'Milano', regional_gamma['Lombardia'])
        add_i_star('dpc-covid19-ita-province', 'Monza e della Brianza', regional_gamma['Lombardia'])
        add_i_star('dpc-covid19-ita-province', 'Napoli', regional_gamma['Campania'])
        add_i_star('dpc-covid19-ita-province', 'Reggio di Calabria', regional_gamma['Calabria'])
        add_i_star('dpc-covid19-ita-province', 'Roma', regional_gamma['Lazio'])
        add_i_star('dpc-covid19-ita-province', 'Savona', regional_gamma['Liguria'])
        add_i_star('dpc-covid19-ita-province', 'Torino', regional_gamma['Piemonte'])
        add_i_star('dpc-covid19-ita-province', 'Verbano-Cusio-Ossola', regional_gamma['Piemonte'])
        create_chart_with_all_new_columns([
            ('dpc-covid19-ita-province', 'Bergamo'),
            ('dpc-covid19-ita-province', 'Bologna'),
            ('dpc-covid19-ita-province', 'Brescia'),
            ('dpc-covid19-ita-province', 'Milano'),
            ('dpc-covid19-ita-province', 'Monza e della Brianza'),
            ('dpc-covid19-ita-province', 'Napoli'),
            ('dpc-covid19-ita-province', 'Reggio di Calabria'),
            ('dpc-covid19-ita-province', 'Roma'),
            ('dpc-covid19-ita-province', 'Savona'),
            ('dpc-covid19-ita-province', 'Torino'),
            ('dpc-covid19-ita-province', 'Verbano-Cusio-Ossola'),
        ], I_STAR)
        add_rt_star('dpc-covid19-ita-province', 'Bergamo')
        add_rt_star('dpc-covid19-ita-province', 'Bologna')
        add_rt_star('dpc-covid19-ita-province', 'Brescia')
        add_rt_star('dpc-covid19-ita-province', 'Milano')
        add_rt_star('dpc-covid19-ita-province', 'Monza e della Brianza')
        add_rt_star('dpc-covid19-ita-province', 'Napoli')
        add_rt_star('dpc-covid19-ita-province', 'Reggio di Calabria')
        add_rt_star('dpc-covid19-ita-province', 'Roma')
        add_rt_star('dpc-covid19-ita-province', 'Savona')
        add_rt_star('dpc-covid19-ita-province', 'Torino')
        add_rt_star('dpc-covid19-ita-province', 'Verbano-Cusio-Ossola')
        create_chart_with_all_new_columns([
            ('dpc-covid19-ita-province', 'Bergamo'),
            ('dpc-covid19-ita-province', 'Bologna'),
            ('dpc-covid19-ita-province', 'Brescia'),
            ('dpc-covid19-ita-province', 'Milano'),
            ('dpc-covid19-ita-province', 'Monza e della Brianza'),
            ('dpc-covid19-ita-province', 'Napoli'),
            ('dpc-covid19-ita-province', 'Reggio di Calabria'),
            ('dpc-covid19-ita-province', 'Roma'),
            ('dpc-covid19-ita-province', 'Savona'),
            ('dpc-covid19-ita-province', 'Torino'),
            ('dpc-covid19-ita-province', 'Verbano-Cusio-Ossola'),
        ], RT_STAR)

"""Django admin management module for loading Italian DPC Covid19 Italy data set."""

import io

import requests
from django.core.management.base import BaseCommand
from charts.repositories.model_repository import ChartRepository
from charts.repositories.repository_factory import create_read_repository
from charts.usecases.usecase_factory import create_import_usecase

ROOT = 'https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/'
NATIONAL = 'dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv'
PROVINCE = 'dati-province/dpc-covid19-ita-province.csv'
REGION = 'dati-regioni/dpc-covid19-ita-regioni.csv'


def load_data(csv_path, chart_name, transformation, group_by_col, summary_cols):
    print('Loading {}'.format(csv_path))
    response = requests.get(ROOT + csv_path)
    file_like_obj = io.BytesIO(response.content)
    file_like_obj.name = csv_path.split('/')[1]
    repo = create_read_repository(file_like_obj, chart_name, '', 'D')
    import_use_case = create_import_usecase(transformation, repo.read(), group_by_col, summary_cols)
    entities = import_use_case.execute()
    for entity in entities:
        ChartRepository.write(entity)


class Command(BaseCommand):
    help = 'Load Italian DPC Covid19 Italy data set.'

    def handle(self, *args, **options):
        load_data(NATIONAL, 'Andamento Nazionale', 'TABULAR', '', '')
        load_data(REGION, '', 'GROUPBY', 'denominazione_regione', 'totale_positivi terapia_intensiva')
        load_data(PROVINCE, '', 'GROUPBY', 'denominazione_provincia', 'totale_casi')

"""datatoy URL Configuration

Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

import charts.views

urlpatterns = [
    url(r'^$', charts.views.IndexView.as_view(), name='index'),
    url(r'^display/(?P<chart_id>[0-9]+)/$', charts.views.display, name="display"),
    url(r'^dump/(?P<slug_title>[a-z0-9-]+)/$', charts.views.dump, name="dump"),
    url(r'^export/$', charts.views.ExportView.as_view(), name='export'),
    url(r'^import/$', charts.views.import_file, name="import"),
    url(r'^query/(?P<chart_id>[0-9]+)/$', charts.views.query, name="query"),
]

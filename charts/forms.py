from django import forms


class QueryForm(forms.Form):
    """Form for queries on charts displaying."""

    check_all = forms.BooleanField(label="Check All", required=False)

    def __init__(self, y_labels, xs, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.y_labels = y_labels
        for y_label in y_labels:
            self.fields[y_label] = forms.BooleanField(required=False)
        self.fields["from"] = forms.DateField(label="From", required=False, initial=xs[0])
        self.fields["to"] = forms.DateField(label="To", required=False, initial=xs[-1])
        self.start_date = xs[0]
        self.end_date = xs[-1]
        self.fields["operation"] = forms.ChoiceField(choices=(('ORIG', 'raw data'),
                                                              ('PERCENT', 'percentage'),
                                                              ('VAR', 'growth rate'),
                                                              ))
        self.fields["chart_style"] = forms.ChoiceField(choices=(('LINES', 'lines'),
                                                                ('DOTS', 'dots'),
                                                                ('LINE_DOTS', 'lines with dots'),
                                                                ('LINES_LEGEND', 'lines with legend'),
                                                                ))
        self.fields["tool"] = forms.ChoiceField(choices=(('NONE', '--'),
                                                         ('SMA', 'moving average'),
                                                         ('LR', 'logistic fit'),
                                                         ('DAY0', 'day 0 prediction')))
        self.fields["window_size"] = forms.ChoiceField(choices=((3, '3'), (4, '4'), (5, '5'),
                                                                (7, '7'), (14, '14')))
        self.fields["log_y"] = forms.BooleanField(label="Log y", required=False)

    def clean_from(self):
        """'from' date validator."""
        from_ = self.cleaned_data["from"]
        if from_ < self.start_date:
            raise forms.ValidationError("Date cannot be less than %s" % self.start_date)
        return from_

    def clean_to(self):
        """'to' date validator."""
        to = self.cleaned_data["to"]
        if to > self.end_date:
            raise forms.ValidationError("Date cannot be more than %s" % self.end_date)
        return to

    def clean(self):
        """For now validates that at least one checkbox is checked."""
        cleaned_data = super(QueryForm, self).clean()
        for y_label in self.y_labels:
            if cleaned_data[y_label]:
                return None
        raise forms.ValidationError("At least one checkbox must be checked")


class ImportForm(forms.Form):
    """Form for importing data into model."""
    import_filename = forms.FileField(label='Data File')
    sheet_name = forms.CharField(label='Excel Data Sheet', required=False)
    y_label = forms.CharField(label='Unit of y axes', required=False)
    period = forms.ChoiceField(choices=(('D', 'date'), ('Y', 'year')))
    transformation = forms.ChoiceField(choices=(('TABULAR', 'tabular'), ('GROUPBY', 'groupby')))
    group_by_col_name = forms.CharField(label='Group aggregation column name', required=False)
    summary_col_name = forms.CharField(label='Groups summary column names space separated',
                                       required=False)

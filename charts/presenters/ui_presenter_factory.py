"""Factories for displayable objects"""

import charts.presenters.ui_presenters as presenters


class PresenterSelectionNotFound(Exception):
    """A presenter selection was not found."""


def create_presenter(presenter_selection, usecase, is_log_y):
    """Factory function.
    :param presenter_selection: str
    :param usecase: usecase
    :param is_log_y: Boolean
    :return: presenters.*
    """
    if presenter_selection == 'LINES':
        return presenters.LinesPresenter(usecase, is_log_y)
    elif presenter_selection == 'DOTS':
        return presenters.DotsPresenter(usecase, is_log_y)
    elif presenter_selection == 'LINE_DOTS':
        return presenters.LinesDotsPresenter(usecase, is_log_y)
    elif presenter_selection == 'LINES_LEGEND':
        return presenters.LinePresenterWithLegend(usecase, is_log_y)
    else:
        raise PresenterSelectionNotFound

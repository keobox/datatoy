"""Encapsulate logic to return report objects for a particular usecase."""

from io import BytesIO

import tablib


class DataSetPresenter(object):
    """Encapsulation for tablib data set objects."""

    def __init__(self, response):
        """Constructor.
        :param response: dict
        """
        self.response = response

    def get_data_set(self):
        """ Returns a Dataset.
        :returns Dataset
        """
        data_set = tablib.Dataset(title=self.response['name'])
        df = self.response['columns']
        heads = list(df.keys())
        first_header = heads[0]
        for v in df[first_header]:
            try:
                # first column is datetime
                data_set.append([v.isoformat()])
            except AttributeError:
                data_set.append([v])
        data_set.headers = [first_header]
        for col in heads[1:]:
            data_set.append_col(df[col], header=col)
        return data_set


class DataBookPresenter(object):
    """Encapsulation for tablib data set objects."""

    def __init__(self, responses):
        """Constructor.
        :param responses: list of dict
        """
        self.response_list = responses

    def get_data_book(self):
        """Returns a Databook.
        :return: Databook
        """
        data_book = tablib.Databook()
        for resp in self.response_list:
            data_set_presenter = DataSetPresenter(resp)
            data_book.add_sheet(data_set_presenter.get_data_set())
        return data_book


def data_book_to_xls_stream(data_book):
    """Return a stream containing xls data of a data book.
    :param data_book:
    :return: str
    """
    stream = BytesIO()
    stream.write(data_book.xls)
    return stream.getvalue()

"""Encapsulate logic to return chart objects suitable for web UI
for a particular use case response."""

from itertools import cycle

import bokeh.models.tools as tools
from bokeh.palettes import Category20_20 as palettes
from bokeh.plotting import ColumnDataSource
from bokeh.plotting import figure
import pandas as pd


def _get_tooltips(column):
    """
    Returns hover tool tooltips
    """
    return [('(x, y)', '(@x_date_formatted, $y)'), ('column', '%s' % column)]


def _get_hover_tool(column, renderer):
    """
    Returns hover tool
    """
    tool = tools.HoverTool(tooltips=_get_tooltips(column),
                           renderers=[renderer], line_policy='nearest')
    tool.point_policy = 'snap_to_data'
    tool.line_policy = 'none'
    return tool


class LinesPresenter:
    """This presenter use the mid-level bokeh API in order to build a Chart
       with the ability to display the column names in the tooltips of the points."""

    def __init__(self, response, is_log_y):
        """Constructor.
        :param usecase: use case object
        :param is_log_y: Boolean
        """
        self.response = response
        self.df = self.response['columns']
        self.y_labels = self.response['y_labels']
        self.dot_y_labels = self.response.get('dots', [])
        self.x_label = self.response['x_label']
        self.title = self.response['title']
        self.y_label = self.response['y_label']
        self.tools = [tools.PanTool(),
                      tools.BoxZoomTool(),
                      tools.WheelZoomTool(),
                      tools.ResetTool()]
        if is_log_y:
            self.y_axis_type = 'log'
        else:
            self.y_axis_type = 'linear'
        colors = palettes
        self.colorcycler = cycle(colors)
        self.df = pd.DataFrame.from_dict(self.df)
        self.source = ColumnDataSource(self.df)
        self.source.add(self.df[self.x_label].apply(lambda d: d.strftime('%Y-%m-%d')),
                        'x_date_formatted')
        self.figure_obj = figure(plot_width=800, plot_height=600,
                                 title=self.title, tools=self.tools,
                                 x_axis_label=self.x_label, y_axis_label=self.y_label,
                                 x_axis_type='datetime', y_axis_type=self.y_axis_type)

    def get_displayable_object(self):
        """Returns the displayable chart for a particular usecase.
        :return: Figure
        """
        for column in self.y_labels:
            line = self.figure_obj.line(x=self.x_label, y=column, source=self.source,
                                        line_color=next(self.colorcycler), line_width=2,
                                        line_cap='round', line_join='round')
            hover_tool = _get_hover_tool(column, line)
            self.figure_obj.add_tools(hover_tool)
        for column in self.dot_y_labels:
            point = self.figure_obj.circle(x=self.x_label, y=column, source=self.source,
                                           fill_color=next(self.colorcycler), size=5)
            hover_tool = _get_hover_tool(column, point)
            self.figure_obj.add_tools(hover_tool)
        return self.figure_obj


class LinePresenterWithLegend(LinesPresenter):
    """This presenter use the mid-level bokeh API in order to build a Chart
       with the ability to display the column names in the tooltips of the points
       and with interactive legend."""

    def __init__(self, response, is_log_y):
        """Constructor.
        :param response: dict
        :param is_log_y: Boolean
        """
        super().__init__(response, is_log_y)

    def calculate_legend_position(self):
        """Place the legend in a automatic way."""
        df = self.df.copy()
        all_cols = (df[col].fillna(0) for col in (self.y_labels + self.dot_y_labels))
        all_max = [max(x) for x in zip(*all_cols)]
        max_pos = all_max.index(max(all_max))
        if max_pos < len(all_max) // 2:
            return 'top_right'
        return 'top_left'

    def get_displayable_object(self):
        """Returns the displayable chart for a particular usecase.
        :return: Figure
        """
        for column in self.y_labels:
            line = self.figure_obj.line(x=self.x_label, y=column, source=self.source,
                                        line_color=next(self.colorcycler), line_width=2,
                                        line_cap='round', line_join='round',
                                        legend_label=column)
            hover_tool = _get_hover_tool(column, line)
            self.figure_obj.add_tools(hover_tool)
        for column in self.dot_y_labels:
            point = self.figure_obj.circle(x=self.x_label, y=column, source=self.source,
                                           fill_color=next(self.colorcycler), size=5)
            hover_tool = _get_hover_tool(column, point)
            self.figure_obj.add_tools(hover_tool)
        self.figure_obj.legend.location = self.calculate_legend_position()
        self.figure_obj.legend.click_policy = "hide"
        return self.figure_obj


class DotsPresenter(LinesPresenter):
    """This presenter use the mid-level bokeh API in order to build a Chart
       using dots instead of a line with the ability
       to display the column names in the tooltips of the points."""

    def __init__(self, response, is_log_y):
        """Constructor.
        :param response : dict
        :param is_log_y: Boolean
        """
        super().__init__(response, is_log_y)

    def get_displayable_object(self):
        """Returns the displayable chart for a particular usecase.
        :return: Figure
        """
        for column in self.y_labels:
            point = self.figure_obj.circle(x=self.x_label, y=column, source=self.source,
                                           fill_color=next(self.colorcycler), size=5)
            hover_tool = _get_hover_tool(column, point)
            self.figure_obj.add_tools(hover_tool)
        return self.figure_obj


class LinesDotsPresenter(LinesPresenter):
    """This presenter use the mid-level bokeh API in order to build a Chart
       using dots and lines with the ability
       to display the column names in the tooltips of the points."""

    def __init__(self, response, is_log_y):
        """Constructor.
        :param response: dict
        :param is_log_y: Boolean
        """
        super().__init__(response, is_log_y)

    def get_displayable_object(self):
        """Returns the displayable chart for a particular usecase.
        :return: Figure
        """
        for column in self.y_labels:
            color = next(self.colorcycler)
            point = self.figure_obj.circle(x=self.x_label, y=column,
                                           source=self.source,
                                           fill_color=color, size=5)
            hover_tool = _get_hover_tool(column, point)
            self.figure_obj.add_tools(hover_tool)
            self.figure_obj.line(x=self.x_label, y=column, source=self.source,
                                 line_color=color, line_width=2,
                                 line_cap='round', line_join='round')
        return self.figure_obj

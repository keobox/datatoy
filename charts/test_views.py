"""Tests for Django views."""

import datetime as dt
import django.test

import datatoy.wsgi
import datatoy.settings

from django.contrib.auth.models import User

from charts.entities.chart_entity import ChartDataEntity
from charts.repositories.model_repository import ChartRepository


class TestDjangoViews(django.test.TestCase):

    def setUp(self):
        self.client = django.test.Client()
        self.user = User.objects.create_user(username='test_user')
        datatoy.settings.ALLOWED_HOSTS.append('testServer')
        xs = [dt.date(x, 1, 1) for x in [2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013]]
        entity = ChartDataEntity(1,
                                 'Import',
                                 'Bottled',
                                 xs,
                                 'Anni',
                                 [[94.6, 113.98, 125.84, 114.91, 120.6, 145.958, 164.206, 208.525],
                                  [50.87, 54.19, 57.1, 57.49, 66.3, 87.152, 92.764, 102.777]],
                                 ['Italia', 'Francia'],
                                 'Mln Lt')
        ChartRepository.write(entity)

    def test_index(self):
        response = self.client.get('/charts/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Bottled')

    def test_import(self):
        response = self.client.get('/charts/import/')
        self.assertEqual(response.status_code, 302)
        self.client.force_login(self.user)
        response = self.client.get('/charts/import/')
        self.assertEqual(response.status_code, 200)

    def test_export(self):
        response = self.client.get('/charts/export/')
        self.assertEqual(response.status_code, 200)

    def test_query(self):
        repo = ChartRepository(1)
        entity = repo.find_by_title_and_name('Import', 'Bottled')
        response = self.client.get('/charts/query/{}/'.format(entity.id))
        self.assertEqual(response.status_code, 200)

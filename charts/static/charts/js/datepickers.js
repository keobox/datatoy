/**
 * Created by ceplacan on 25/03/16.
 */

$(function () {
    $('#id_from').datepicker({showAnim: 'clip', dateFormat: 'yy-mm-dd'});
    $('#id_from').change(function() {
       $(this).attr('value', $('#id_from').val())
    });

    $('#id_to').datepicker({showAnim: 'clip', dateFormat: 'yy-mm-dd'});
    $('#id_to').change(function() {
        $(this).attr('value', $('#id_to').val())
    });
});

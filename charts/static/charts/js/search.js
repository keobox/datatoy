$('#search').keyup(function () {
    var valThis = $(this).val().toLowerCase();
    if (valThis == "") {
        $('ul.navList > li').show();
    } else {
        $('ul.navList > li').each(function () {
            var text = $(this).text().toLowerCase();
            (text.indexOf(valThis) >= 0) ? $(this).show() : $(this).hide();
        });
    };
});
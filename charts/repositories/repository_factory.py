"""Repository factory functions."""

from __future__ import unicode_literals

import os
from charts.repositories.excel_repository import ExcelRepository
from charts.repositories.csv_repository import TimeSeriesTabularCsvReadRepository


class NotSupportedFileType(Exception):
    """Data file type not supported yet."""


def create_read_repository(file_object, chart_name, y_label, period_type):
    """
    Return chart repository given a file object.
    """
    filename, extension = os.path.splitext(file_object.name)
    if extension == ".xls":
        return ExcelRepository(file_object, chart_name, y_label, period_type)
    if extension == ".csv":
        return TimeSeriesTabularCsvReadRepository(file_object, filename,
                                                  chart_name,
                                                  y_label, period_type)
    raise NotSupportedFileType

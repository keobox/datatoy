"""Django Models repository."""

import collections

import charts.entities.chart_entity as entity
import charts.models as model


def ordered_set(seq):
    """Returns a set that remembers insertion order in seq.
    :param seq: input sequence.
    """
    d = collections.OrderedDict()
    for e in seq:
        d[e] = 0
    return list(d.keys())


class ChartRepository(object):
    """DB repository for Entities
    """

    def __init__(self, id_):
        self.id = id_

    def __read(self, chart):
        data = model.Data.objects.filter(chart__id=chart.id)
        y_labels = ordered_set([x.column for x in data])
        xms = model.Abscissa.objects.filter(chart__id=chart.id)
        xs = [x.x for x in xms]
        ys = []
        for y_label in y_labels:
            ys.append([y.y for y in data if y.column == y_label])
        chart_entity = entity.ChartDataEntity(self.id,
                                              chart.title,
                                              chart.name,
                                              xs,
                                              chart.x_label,
                                              ys,
                                              y_labels,
                                              chart.y_label)
        return chart_entity

    def read(self):
        """Read data from DB
        :return: ChartDataEntity
        """
        chart = model.Chart.objects.filter(id=self.id).get()
        return self.__read(chart)

    def find_by_title_and_name(self, title, name):
        """Returns a chart entity by title and name"""
        chart = model.Chart.objects.filter(title=title, name=name).get()
        self.id = chart.id
        return self.__read(chart)

    @staticmethod
    def write(_entity):
        """Write a entity into DB
        :param _entity: ChartDataEntity
        """
        chart = model.Chart()
        chart.name = _entity.name
        chart.title = _entity.title
        chart.x_label = _entity.x_label
        chart.y_label = _entity.y_label
        chart.save()
        xs_model_objects = []
        for x in _entity.xs:
            abscissa = model.Abscissa()
            abscissa.x = x
            abscissa.chart = chart
            xs_model_objects.append(abscissa)
        model.Abscissa.objects.bulk_create(xs_model_objects)
        data_model_objects = []
        for y_name, y_axis in zip(_entity.y_labels, _entity.ys):
            for y in y_axis:
                d = model.Data()
                d.column = y_name
                d.y = y
                d.chart = chart
                data_model_objects.append(d)
        model.Data.objects.bulk_create(data_model_objects)


class ChartSetRepository(object):
    """Returns a set of entities to use cases.
    """

    def __init__(self, slug):
        self.slug = slug

    def read(self):
        """Returns a set of entities given a slug field.
        :return: list of ChartDataEntity
        """
        charts_ = model.Chart.objects.filter(slug_title=self.slug)
        entities = []
        for chart in charts_:
            repo = ChartRepository(chart.id)
            entities.append(repo.read())
        return entities

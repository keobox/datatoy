"""Repository for comma separated value files."""

import csv
import codecs

from charts.entities.chart_entity import ChartDataEntity
from charts.entities.common_entity_util import transpose


class TimeSeriesTabularCsvReadRepository():
    """
    Repository for comma separated values files.
    """

    def __init__(self, file_object, title, chart_name, y_label, period):
        self.chart_name = chart_name[:30]
        if not chart_name:
            self.chart_name = 'Sheet1'
        self.col_names = None
        self.file_object = file_object
        self.period = period
        self.title = title
        self.reader = None
        self.x_label = None
        self.xs = []
        self.y_label = y_label
        self.ys = []

    def read(self):
        """
        Returns a chart entity: entity.ys are the rows in tabular format
        """
        self.reader = csv.reader(codecs.iterdecode(self.file_object, "utf-8"), dialect=csv.excel)
        for i, line in enumerate(self.reader):
            if i == 0:
                self.x_label = line[0]
                self.col_names = line[1:]
            else:
                if line:
                    if self.period == "D":
                        line[0] = line[0].split("T")[0]
                    self.xs.append(line[0])
                    self.ys.append(line[1:])
        self.ys = transpose(self.ys)
        return ChartDataEntity(0, self.title, self.chart_name, self.xs,
                               self.x_label, self.ys, self.col_names, self.y_label)

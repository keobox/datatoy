import datetime
import os

import charts.repositories.excel_to_data as excel
import charts.entities.chart_entity as entities


def to_date(x, period):
    """Convert a ISO string date or a float in a date object."""
    if period == 'Y':
        return datetime.date(int(x), 1, 1)
    else:
        dt = datetime.datetime.strptime(x, '%Y-%m-%d')
        return datetime.date(dt.year, dt.month, dt.day)


class ExcelRepository(object):
    """Get a filename and a sheetname and return a domain object."""

    def __init__(self, fileobj, sheetname, y_unit_name, period):
        self.entity = None
        self.fileobj = fileobj
        self.period = period
        self.sheetname = sheetname
        self.y_unit_name = y_unit_name

    def read(self):
        """Returns entity."""
        xs, x_label, ys, y_labels = excel.extract_data_from_stream(self.fileobj.read(),
                                                                   self.sheetname)
        x_dates = [to_date(x, self.period) for x in xs]
        title = os.path.splitext(os.path.split(self.fileobj.name)[1])[0]
        return entities.ChartDataEntity(0, title, self.sheetname, x_dates,
                                        x_label, ys, y_labels, self.y_unit_name)

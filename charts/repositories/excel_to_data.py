"""Convert excel file into data."""

from __future__ import unicode_literals

import csv
import mmap
import os

import xlrd


def _extract_data_from_work_book(book, sheet_name):
    data_sheet = book.sheet_by_name(sheet_name)
    _data = []
    for c in range(data_sheet.ncols):
        _data.append(data_sheet.col(c))
    x = [v.value for v in _data[0][1:]]
    x_label = _data[0][0].value
    ys = [y[1:] for y in _data[1:]]
    ys = [[z.value for z in y] for y in ys]
    y_labels = [y[0].value for y in _data[1:]]
    return x, x_label, ys, y_labels


def extract_data(xls, sheet_name):
    """Returns the 'data' sheet of the 'xls' excel file.
    :param xls: name of the excel file
    :param sheet_name: name of the excel sheet
    """
    book = xlrd.open_workbook(xls)
    return _extract_data_from_work_book(book, sheet_name)


def get_work_book_from_file(file_obj):
    """Returns a excel work book given an opened excel file.
    :param file_obj: file obj of an opened excel file
    """
    return xlrd.open_workbook(file_contents=mmap.mmap(file_obj.fileno(), 0))


def get_work_book_from_stream(str_obj):
    """Returns a excel work book given an opened excel file.
    :param str_obj: file content of excel file
    """
    return xlrd.open_workbook(filename=None, file_contents=str_obj)


def extract_data_from_file(file_obj, sheet_name):
    """Returns the 'data' sheet of the 'xls' excel file.
    :param file_obj: file object
    :param sheet_name: string
    :return: tuple
    """
    book = get_work_book_from_file(file_obj)
    return _extract_data_from_work_book(book, sheet_name)


def extract_data_from_stream(str_obj, sheet_name):
    """Returns the 'data' sheet of the 'xls' excel file.
    :param str_obj: content of file.read()
    :param sheet_name: string
    :return:
    """
    book = get_work_book_from_stream(str_obj)
    return _extract_data_from_work_book(book, sheet_name)


def to_csvs(xls, sheet_name, chart_id=1, data_start_id=1, abscissa_start_id=1):
    """Converts a xls sheet to three lists suitable to produce csvs.
    :param xls: name of the excel file
    :param sheet_name: name of the excel sheet
    :param abscissa_start_id: first id (integer pk) of the abscissa
    :param data_start_id: first id of first y axes
    :param chart_id: id (integer pk)
    """
    x, x_label, ys, y_labels = extract_data(xls, sheet_name)
    chart_list = [['id', 'name', 'title', 'x_label', 'y_label'],
                  [chart_id, sheet_name, os.path.splitext(xls)[0], x_label, 'Milioni di litri']]
    data_list = [['id', 'country', 'y', 'graph']]
    data_id = data_start_id
    for column, y in zip(y_labels, ys):
        for y_val in y:
            data_list.append([data_id, column, y_val, chart_id])
            data_id += 1
    abscissa_list = [['id', 'x', 'graph']]
    x_id = abscissa_start_id
    for x_val in x:
        abscissa_list.append([x_id, '%s' % x_val, chart_id])
        x_id += 1
    return chart_list, data_list, abscissa_list


if __name__ == '__main__':
    charts_, data, abscissa = to_csvs("../tests/repositories/Import Germany.xls", "Bottled")
    with open('graphs.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerows(charts_)
    with open('data.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerows(data)
    with open('abscissa.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerows(abscissa)

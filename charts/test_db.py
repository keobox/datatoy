"""Test DB."""
from __future__ import unicode_literals

import datetime
import os
import sys

from django.test import TestCase

import charts.repositories.excel_repository as excel_repo
import charts.repositories.model_repository as model_repo
import charts.usecases.import_usecases as import_usecases
from charts.models import Chart


class ModelTest(TestCase):
    """Test DB."""

    def setUp(self):
        self.xls_filename = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                         "tests/repositories/Import Germany.xls")
        sys.path.append(os.path.dirname(os.path.abspath(__file__)))
        os.environ['DJANGO_SETTINGS_MODULE'] = 'datatoy.settings'

    def _excel_to_model(self, name, y_label, period):
        with open(self.xls_filename, 'rb') as xls_file:
            er = excel_repo.ExcelRepository(xls_file, name, y_label, period)
            entity = er.read()
            uc = import_usecases.TimeSeriesTabularUsecase(entity)
            entity = uc.execute()[0]
        model_repo.ChartRepository.write(entity)

    def test_db(self):
        """Test DB write/read"""
        self._excel_to_model("Euro Unbottled", "Mln Euro", "D")
        query = Chart.objects.all()
        chart = query[0]
        self.assertEqual(chart.name, "Euro Unbottled")
        self.assertEqual(chart.title, "Import Germany")
        self.assertEqual(chart.slug_title, "import-germany")
        self.assertEqual(chart.y_label, "Mln Euro")
        self.assertEqual(chart.x_label, "Years")
        repo = model_repo.ChartRepository(chart.id)
        chart_entity = repo.read()
        self.assertEqual(chart_entity.name, chart.name)
        self.assertTrue(isinstance(chart_entity.xs[0], datetime.date))

    def test_set_repo(self):
        """Test SetRepository."""
        self._excel_to_model("Euro Unbottled", "Mln Euro", "D")
        self._excel_to_model("Bottled", "Mln Lt", "D")
        entities = model_repo.ChartSetRepository("import-germany").read()
        self.assertEqual(len(entities), 2)
        self.assertEqual(entities[0].name, "Euro Unbottled")
        self.assertEqual(entities[1].name, "Bottled")

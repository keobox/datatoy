import datetime
import unittest.mock
from collections import OrderedDict

from charts.adapters.pandas_adapters import Day0Predictor
from charts.adapters.pandas_adapters import SimpleMovingAverage
from charts.adapters.pandas_adapters import SingleVariableLogisticRegression
from charts.usecases.adapters import DataLibraryAdapter


class TestAdapters(unittest.TestCase):
    def setUp(self):
        cols = {
            'days': ['2020-11-08', '2020-11-09', '2020-11-10', '2020-11-11', '2020-11-12'],
            'col1': [1, 2, 3, 3, 4],
            'col2': [5, 9, 7, 12, 7],
            'col3': [9, 2, 5, 4, 7],
        }
        self.response = {'columns': OrderedDict(cols),
                         'name': 'sma',
                         'title': 'moving averages',
                         'x_label': 'days',
                         'y_label': 'numbers',
                         'y_labels': ['col1', 'col2', 'col3']
                         }

    def test_simple_moving_average(self):
        self.assertTrue(issubclass(SimpleMovingAverage, DataLibraryAdapter))
        data_tool = SimpleMovingAverage(2)
        self.assertTrue(isinstance(data_tool, DataLibraryAdapter))
        processed = data_tool.execute(self.response)
        self.assertTrue(processed)
        self.assertNotEqual(processed, self.response)

    def test_logistic_regression_raise_when_multiple_columns_present(self):
        self.assertTrue(issubclass(SingleVariableLogisticRegression, DataLibraryAdapter))
        data_tool = SingleVariableLogisticRegression(7)
        self.assertTrue(isinstance(data_tool, DataLibraryAdapter))
        self.assertRaises(NotImplementedError, data_tool.execute, self.response)

    def test_logistic_regression_with_single_column(self):
        col = {
            'days': ['2020-11-08', '2020-11-09', '2020-11-10', '2020-11-11',
                     '2020-11-12', '2020-11-13', '2020-11-14', '2020-11-15',
                     '2020-11-16', '2020-11-17', '2020-11-18'],
            'col': [1, 2, 3, 3, 4, 8, 16, 32, 64, 128, 256],
        }
        col['days'] = [datetime.datetime.strptime(d, '%Y-%m-%d').date() for d in col['days']]
        response = {'columns': OrderedDict(col),
                    'name': 'lr',
                    'title': 'logistic regression',
                    'x_label': 'days',
                    'y_label': 'numbers',
                    'y_labels': ['col']
                    }
        data_tool = SingleVariableLogisticRegression(7)
        processed = data_tool.execute(response)
        self.assertEqual(len(processed['columns']), 3)
        self.assertEqual(processed['y_labels'], ['col', 'col_logistic'])

    @unittest.mock.patch('charts.adapters.pandas_adapters.fsolve')
    def test_day0_prediction(self, fsolve):
        col = {
            'days': ['2020-11-08', '2020-11-09', '2020-11-10', '2020-11-11',
                     '2020-11-12', '2020-11-13', '2020-11-14', '2020-11-15',
                     '2020-11-16', '2020-11-17', '2020-11-18'],
            'col': [1, 2, 3, 3, 4, 8, 16, 32, 64, 128, 256],
        }
        col['days'] = [datetime.datetime.strptime(d, '%Y-%m-%d').date() for d in col['days']]
        response = {'columns': OrderedDict(col),
                    'name': 'day0',
                    'title': 'day 0 prediction',
                    'x_label': 'days',
                    'y_label': 'numbers',
                    'y_labels': ['col']
                    }
        data_tool = Day0Predictor(7)
        fsolve.return_value = [300]
        processed = data_tool.execute(response)
        self.assertEqual(len(processed['columns']), 4)
        self.assertEqual(processed['y_labels'], ['col', 'col_logistic'])
        self.assertEqual(processed['dots'], ['col_day0'])
        self.assertEqual(len(processed['columns']['col_day0']), 300)

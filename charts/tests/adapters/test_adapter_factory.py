import unittest
import charts.adapters.adapter_factory as factory
from charts.usecases.adapters import DataLibraryAdapter


class TestAdapterCreate(unittest.TestCase):

    def test_create_simple_moving_average(self):
        adapter = factory.create_adapter('SMA', window_size=2)
        self.assertTrue(isinstance(adapter, DataLibraryAdapter))
        self.assertEqual(adapter.window_size, 2)

    def test_create_logistic_regression(self):
        adapter = factory.create_adapter('LR', window_size=7)
        self.assertTrue(isinstance(adapter, DataLibraryAdapter))
        self.assertEqual(adapter.window_size, 7)

    def test_create_day0_predictor(self):
        adapter = factory.create_adapter('DAY0', window_size=7)
        self.assertTrue(isinstance(adapter, DataLibraryAdapter))
        self.assertEqual(adapter.window_size, 7)

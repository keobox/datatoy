"""Tests for use cases. Uses cases are business logic accordingly with R. Martin clean architecture."""

from __future__ import unicode_literals

import datetime
import os
import unittest
import unittest.mock
from abc import ABC
from collections import OrderedDict

import charts.entities.chart_entity as entities
import charts.repositories.excel_to_data as excel
import charts.usecases.chart_usecases as usecases
import charts.usecases.import_usecases as import_usecases
from charts.usecases.adapters import DataLibraryAdapter


def str_to_date(xs):
    """Returns a list of date objects
    :param xs: list of string in date ISO format
    :return: list of datetime.date
    """
    return [datetime.datetime.strptime(x, '%Y-%m-%d') for x in xs]


def _load_data(id_, xls_filename, sheet_name, y_label):
    title = os.path.split(xls_filename)[-1]
    title = os.path.splitext(title)[0]
    x, x_label, ys, y_labels = excel.extract_data(xls_filename, sheet_name)
    x = str_to_date(x)
    ys_gaps = [import_usecases.fill_gaps_with_null(yy) for yy in ys]
    return (x, entities.ChartDataEntity(id_, title, sheet_name, x, x_label,
                                        ys_gaps, y_labels, y_label))


class TestUsecases(unittest.TestCase):
    """Test the domain logic."""

    # the use case should take all the logic
    # in view.py into itself
    # the idea is to pass entities to use cases (no mocks for model repository),
    # while excel repo can be used as it is (at least for now)

    def setUp(self):
        xls_filename = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                    "../repositories/Import Germany.xls")
        self.x, self.chart = _load_data(1, xls_filename, "Euro Unbottled", "Mln Euro")
        second_chart = _load_data(2, xls_filename, "Bottled", "Mln Lt")[1]
        # use "set" because of an iterable with unique elements is assumed
        # the set is also ordered (insertion order respected)
        self.chart_set = [self.chart, second_chart]

    def test_filterable_chart_usecase_without_filter(self):
        """Test for filterable charts."""
        uc = usecases.FilterableChartUseCase(self.chart)
        response = uc.execute()
        df = response['columns']
        self.assertEqual(len(df['Years']), 10)
        self.assertEqual(len(df.keys()), 17)

    def test_filterable_chart_usecase_y_label(self):
        """Test y_label for filterable use case."""
        uc = usecases.FilterableChartUseCase(self.chart)
        self.assertEqual(uc.y_label, self.chart.y_label)

    def test_filterable_chart_usecase_with_filter(self):
        """Test for filterable charts."""
        uc = usecases.FilterableChartUseCase(self.chart)
        uc.filter(['Italy', 'France'], self.x[1], self.x[-2])
        response = uc.execute()
        df = response['columns']
        self.assertEqual(len(df['Years']), 8)
        self.assertEqual(len(df.keys()), 3)

    def test_xs_is_date_without_filter(self):
        """Test that x axis is a date list."""
        uc = usecases.FilterableChartUseCase(self.chart)
        response = uc.execute()
        df = response['columns']
        self.assertTrue(isinstance(uc.filtered_xs[0], datetime.date))
        self.assertTrue(isinstance(df['Years'][0], datetime.date))

    def test_xs_is_date_with_filter(self):
        uc = usecases.FilterableChartUseCase(self.chart)
        uc.filter(['Italy', 'France'], self.x[1], self.x[-2])
        response = uc.execute()
        df = response['columns']
        self.assertTrue(isinstance(uc.filtered_xs[0], datetime.date))
        self.assertTrue(isinstance(df['Years'][0], datetime.date))

    def test_percent_chart_usecase_without_filter(self):
        """Test for percentage logic."""
        uc = usecases.PercentageChartUseCase(self.chart)
        response = uc.execute()
        df = response['columns']
        result = 81 / 286.56 * 100
        entry = df['Italy'][0]
        self.assertEqual(uc.id, 1)
        self.assertTrue(abs(entry - result) < 0.001)

    def test_percentage_chart_usecase_y_label(self):
        """Test y_label for percentage use case."""
        uc = usecases.PercentageChartUseCase(self.chart)
        self.assertNotEqual(uc.y_label, self.chart.y_label)

    def test_percentage_chart_usecase_with_filter(self):
        """Test for percentage logic."""
        uc = usecases.PercentageChartUseCase(self.chart)
        uc.filter(['Italy', 'France'], self.x[1], self.x[-2])
        response = uc.execute()
        df = response['columns']
        result = 94.7 / 294.81 * 100
        entry = df['Italy'][0]
        self.assertEqual(uc.id, 1)
        self.assertTrue(abs(entry - result) < 0.001)
        self.assertEqual(len(df['Years']), 8)
        self.assertEqual(len(df.keys()), 3)

    def test_percentage_chart_usecase_with_gaps(self):
        """Test for percentage logic."""
        uc = usecases.PercentageChartUseCase(self.chart)
        uc.filter(['Italy', 'Greece'], self.x[1], self.x[-2])
        response = uc.execute()
        df = response['columns']
        result = 0
        entry = df['Greece'][0]
        self.assertTrue(abs(entry - result) < 0.001)

    def test_percentage_variation_usecase_with_filter(self):
        """Test for percentage variation."""
        uc = usecases.PercentageVariationChartUseCase(self.chart)
        result = (114 - 94.7) * 100 / 94.7
        uc.filter(['Italy', 'France'], self.x[1], self.x[-2])
        response = uc.execute()
        df = response['columns']
        entry = df['Italy'][0]
        self.assertTrue(abs(entry - result) < 0.001)
        self.assertEqual(len(df['Years']), 7)
        self.assertEqual(len(df['Italy']), 7)
        self.assertEqual(len(df.keys()), 3)

    def test_percentage_variation_usecase_without_filter(self):
        """Test for percentage variation."""
        uc = usecases.PercentageVariationChartUseCase(self.chart)
        result = (94.7 - 81.0) * 100 / 81.0
        response = uc.execute()
        df = response['columns']
        entry = df['Italy'][0]
        self.assertTrue(abs(entry - result) < 0.001)
        self.assertEqual(len(df['Years']), 9)
        self.assertEqual(len(df['Italy']), 9)
        self.assertEqual(len(df.keys()), 17)

    def test_percentage_variation_chart_usecase_y_label(self):
        """Test y_label for percentage use case."""
        uc = usecases.PercentageVariationChartUseCase(self.chart)
        self.assertNotEqual(uc.y_label, self.chart.y_label)

    def test_chart_set(self):
        """Test chart set use case."""
        uc = usecases.ChartSetUseCase(self.chart_set)
        response_set = uc.get_data_responses()
        self.assertEqual(len(response_set), 2)
        self.assertIsNotNone(response_set[0]['columns']['Italy'])
        self.assertIsNotNone(response_set[1]['columns']['France'])

    def test_tabular_import_usecase(self):
        """Test the tabular import usecase."""
        data_entity = entities.ChartDataEntity(3, "Simple entity", "Chart name",
                                               ["2020-02-24", "2020-02-25"], "data",
                                               [[10, 11],
                                                [20, 22]],
                                               ["col1", "col2"], "unit")
        uc = import_usecases.TimeSeriesTabularUsecase(data_entity)
        entity_list = uc.execute()
        self.assertEqual(len(entity_list), 1)
        self.assertEqual(entity_list[0].y_labels, ["col1", "col2"])
        self.assertEqual(entity_list[0].ys[0][0], 10)
        self.assertEqual(entity_list[0].ys[0][1], 11)
        self.assertEqual(entity_list[0].ys[1][0], 20)
        self.assertEqual(entity_list[0].ys[1][1], 22)

    def test_tabular_import_usecase_remove_cols(self):
        data_entity = entities.ChartDataEntity(4, "Simple entity", "Chart name",
                                               ["2020-02-24", "2020-02-25"], "data",
                                               [[None, None],
                                                [20, 22]],
                                               ["col1", "col2"], "unit")
        uc = import_usecases.TimeSeriesTabularUsecase(data_entity)
        entity_list = uc.execute()
        self.assertEqual(len(entity_list), 1)
        self.assertEqual(entity_list[0].y_labels, ["col2"])

    def test_groupby_import_usecase(self):
        input_entity = entities.ChartDataEntity(5, "Group entity", "a group",
                                                ["2020-02-24", "2020-02-24", "2020-02-25", "2020-02-25"],
                                                "data",
                                                [["lom", "cal", "lom", "cal"],
                                                 [10, 20, 30, 40],
                                                 [11, 21, 31, 41],
                                                 ["ITA", "ITA", "ITA", "ITA"]],
                                                ["group_by", "col1", "col2", "col3"], "unit")
        uc = import_usecases.TimeSeriesGroupByUsecase(input_entity, "group_by", "col2")
        group1 = entities.ChartDataEntity(6, "Group entity", "lom", ["2020-02-24", "2020-02-25"],
                                          "data", [[10, 30], [11, 31]], ["col1", "col2"], "unit")
        group2 = entities.ChartDataEntity(7, "Group entity", "cal", ["2020-02-24", "2020-02-25"],
                                          "data", [[20, 40], [21, 41]], ["col1", "col2"], "unit")
        data_entities = uc.execute()
        self.assertEqual(len(data_entities), 3)
        self.assertEqual(group1.name, data_entities[0].name)
        self.assertEqual(group2.name, data_entities[1].name)
        self.assertEqual(group1.ys, data_entities[0].ys)
        self.assertEqual(group2.ys, data_entities[1].ys)
        self.assertEqual(group1.y_labels, data_entities[0].y_labels)
        self.assertEqual(group2.y_labels, data_entities[1].y_labels)
        self.assertEqual(group1.xs, data_entities[0].xs)
        self.assertEqual(group2.xs, data_entities[1].xs)
        self.assertEqual("col2", data_entities[2].name)
        self.assertEqual(["lom", "cal"], data_entities[2].y_labels)
        self.assertEqual([[11, 31], [21, 41]], data_entities[2].ys)


class NoOpConcreteAdapter(DataLibraryAdapter):

    def execute(self, response):
        return response


class NotGoodAdapter(DataLibraryAdapter):
    pass


class NoInheritanceAdapter:

    def execute(self, response):
        return response


class WrongMethodAdapter:

    def exec(self, response):
        return response


class TestDataLibraryAdapter(unittest.TestCase):
    """Inject in the chart's use cases adapters to make some data processing
    but isolate data processing library API from use cases' code."""

    def setUp(self):
        self.entity = entities.ChartDataEntity(0, "inject tool", "panda",
                                               ["2020-11-07", "2020-11-08", "2020-11-09"], "days",
                                               [[1, 2, 3], [4, 5, 6], [7, 8, 9]],
                                               ["col1", "col2", "col3"], "numbers")
        self.response = {'columns': OrderedDict(
            [('days', ['2020-11-07', '2020-11-08', '2020-11-09']), ('col1', [1.0, 2.0, 3.0]), ('col2', [4.0, 5.0, 6.0]),
             ('col3', [7.0, 8.0, 9.0])]), 'name': 'panda', 'title': 'inject tool', 'x_label': 'days',
            'y_label': 'numbers', 'y_labels': ['col1', 'col2', 'col3']}

    def test_injection_in_filterable_use_case(self):
        data_tool = unittest.mock.MagicMock()
        uc = usecases.FilterableChartUseCase(self.entity, data_tool)
        uc.execute()
        data_tool.execute.assert_called_with(self.response)

    def test_injection_in_percentage_use_case(self):
        data_tool = unittest.mock.MagicMock()
        uc = usecases.PercentageChartUseCase(self.entity, data_tool)
        uc.execute()
        calls = data_tool.execute.call_args_list
        response = calls[0].args[0]
        self.assertEqual(tuple(self.response['columns']['days']), response['columns']['days'])
        self.assertEqual(self.response['name'], response['name'])
        self.assertEqual(self.response['title'], response['title'])
        data_tool.execute.assert_called_once()

    def test_injection_in_percentage_variation_use_case(self):
        data_tool = unittest.mock.MagicMock()
        uc = usecases.PercentageVariationChartUseCase(self.entity, data_tool)
        uc.execute()
        calls = data_tool.execute.call_args_list
        response = calls[0].args[0]
        self.assertEqual(self.response['columns']['days'][1:], response['columns']['days'])
        self.assertEqual(self.response['name'], response['name'])
        self.assertEqual(self.response['title'], response['title'])
        data_tool.execute.assert_called_once()

    def test_interface_enforcing_instantiation(self):
        self.assertTrue(issubclass(NotGoodAdapter, DataLibraryAdapter))
        self.assertRaises(TypeError, NotGoodAdapter)

    def test_interface_valid_instantiation(self):
        self.assertTrue(issubclass(NoOpConcreteAdapter, DataLibraryAdapter))
        adapter = NoOpConcreteAdapter()
        response = adapter.execute(self.response)
        self.assertEqual(self.response, response)

    def test_interface_subclasshook(self):
        self.assertTrue(issubclass(NoInheritanceAdapter, DataLibraryAdapter))
        self.assertFalse(issubclass(WrongMethodAdapter, DataLibraryAdapter))

    def test_adapter_injection_in_filter_use_case(self):
        self.assertTrue(issubclass(NoOpConcreteAdapter, DataLibraryAdapter))
        adapter = NoOpConcreteAdapter()
        uc = usecases.FilterableChartUseCase(self.entity, adapter)
        response = uc.execute()
        self.assertEqual(self.response, response)

    def test_adapter_injection_in_percentage_use_case(self):
        self.assertTrue(issubclass(NoOpConcreteAdapter, DataLibraryAdapter))
        adapter = NoOpConcreteAdapter()
        uc = usecases.PercentageChartUseCase(self.entity, adapter)
        response = uc.execute()
        self.assertTrue(response)

    def test_adapter_injection_in_percentage_variation_use_case(self):
        self.assertTrue(issubclass(NoOpConcreteAdapter, DataLibraryAdapter))
        adapter = NoOpConcreteAdapter()
        uc = usecases.PercentageVariationChartUseCase(self.entity, adapter)
        response = uc.execute()
        self.assertTrue(response)

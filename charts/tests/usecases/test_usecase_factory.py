"""Tests for usecase factory."""

from __future__ import unicode_literals

import datetime
import unittest.mock

import charts.entities.chart_entity as entity
import charts.usecases.chart_usecases as usecases
import charts.usecases.usecase_factory as factory


def int_to_date(xs):
    """Returns a list of date objects
    :param xs: list of string in date ISO format
    :return: list of datetime.date
    """
    return [datetime.date(x, 12, 31) for x in xs]


class TestUsecaseFactory(unittest.TestCase):
    def setUp(self):
        name = 'Bottled'
        x_label = 'Anni'
        xs = int_to_date([2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013])
        ys = [[94.6, 113.98, 125.84, 114.91, 120.6, 145.958, 164.206, 208.525],
              [50.87, 54.19, 57.1, 57.49, 66.3, 87.152, 92.764, 102.777]]
        y_labels = ['Italy', 'France']
        title = 'Import Germany'
        y_label = 'Mln Lt'
        self.chart = entity.ChartDataEntity(1, title, name, xs, x_label, ys, y_labels, y_label)

    def test_filterable_usecase_selection(self):
        usecase = factory.create_usecase('ORIG', self.chart)
        usecase.filter(['France'], datetime.date(2007, 12, 31), datetime.date(2013, 12, 31))
        self.assertTrue(isinstance(usecase, usecases.FilterableChartUseCase))
        self.assertEqual(usecase.y_label, 'Mln Lt')

    def test_percentage_usecase_selection(self):
        usecase = factory.create_usecase('PERCENT', self.chart)
        usecase.filter(['Italy'], datetime.date(2006, 12, 31), datetime.date(2012, 12, 31))
        self.assertTrue(isinstance(usecase, usecases.PercentageChartUseCase))
        self.assertNotEqual(usecase.y_label, self.chart.y_label)

    def test_variation_usecase_selection(self):
        usecase = factory.create_usecase('VAR', self.chart)
        usecase.filter(['Italy'], datetime.date(2006, 12, 31), datetime.date(2013, 12, 31))
        self.assertTrue(isinstance(usecase, usecases.PercentageVariationChartUseCase))
        self.assertNotEqual(usecase.y_label, self.chart.y_label)

    def test_bad_selection(self):
        try:
            factory.create_usecase('fake', self.chart)
            self.assertTrue(False)
        except factory.UseCaseSelectionNotFound:
            self.assertTrue(True)

    def test_tabular_import_usecase_selection(self):
        usecase = factory.create_import_usecase('TABULAR', self.chart, None, None)
        # check interface
        attr = getattr(usecase, 'execute')
        self.assertIsNotNone(attr)

    def test_groupby_import_usecase_selection(self):
        usecase = factory.create_import_usecase('GROUPBY', self.chart, 'group_by', 'summary_col')
        # check interface
        attr = getattr(usecase, 'execute')
        self.assertIsNotNone(attr)

    def test_data_tool_injection_for_filter_usecase(self):
        data_tool = unittest.mock.MagicMock()
        usecase = factory.create_usecase('ORIG', self.chart, data_tool)
        self.assertTrue(isinstance(usecase, usecases.FilterableChartUseCase))
        data_tool.execute.assert_not_called()

    def test_data_tool_injection_for_percentage_usecase(self):
        data_tool = unittest.mock.MagicMock()
        usecase = factory.create_usecase('PERCENT', self.chart, data_tool)
        self.assertTrue(isinstance(usecase, usecases.PercentageChartUseCase))
        data_tool.execute.assert_not_called()

    def test_data_tool_injection_for_percentage_variation_usecase(self):
        data_tool = unittest.mock.MagicMock()
        usecase = factory.create_usecase('VAR', self.chart, data_tool)
        self.assertTrue(isinstance(usecase, usecases.PercentageVariationChartUseCase))
        data_tool.execute.assert_not_called()

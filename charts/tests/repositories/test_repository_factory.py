"""Test factory function for read repositories."""

from __future__ import unicode_literals

import unittest
import unittest.mock as mock

from charts.repositories.repository_factory import create_read_repository


class TestRepositoryFactory(unittest.TestCase):

    def test_read_repo_factory_excel(self):
        file_obj = mock.Mock()
        file_obj.name = "filename.xls"
        repo = create_read_repository(file_obj, "chart_name", "y_label", "D")
        self.assertIsNotNone(repo)
        # check interface
        self.assertIsNotNone(getattr(repo, "read"))

    def test_read_repo_factory_csv(self):
        file_obj = mock.Mock()
        file_obj.name = "filename.csv"
        repo = create_read_repository(file_obj, "chart_name", "y_label", "D")
        self.assertIsNotNone(repo)
        # check interface
        self.assertIsNotNone(getattr(repo, "read"))


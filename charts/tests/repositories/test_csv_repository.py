import unittest

from io import BytesIO
from charts.repositories.csv_repository import TimeSeriesTabularCsvReadRepository


class TestCsvRepository(unittest.TestCase):
    def test_cvs_repository(self):
        csv_file = BytesIO(b"""\
data,col1,col2
2020-01-31,10,20
2020-02-29,11,22
""")
        repo = TimeSeriesTabularCsvReadRepository(csv_file, "some data", "sheet1", "y label", "D")
        chart = repo.read()
        self.assertIsNotNone(chart)
        self.assertEqual(chart.x_label.strip(), "data")
        self.assertEqual(set(chart.y_labels), set(["col1", "col2"]))
        self.assertEqual(chart.xs[0].strip(), "2020-01-31")
        self.assertEqual(chart.ys[0][1], "11")
        self.assertEqual(chart.ys[1][1], "22")

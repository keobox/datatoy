"""Test functions for extracting data from excel."""

from __future__ import unicode_literals

import os
import unittest

from charts.repositories import excel_to_data


class TestExtractData(unittest.TestCase):
    def setUp(self):
        self.xlsFile = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                    "Import Germany.xls")

    def test_extract_data(self):
        x, x_label, ys, y_labels = excel_to_data.extract_data(self.xlsFile, "Bottled")
        self.assertEqual(len(x), 10)
        self.assertEqual(x_label, "Years")
        self.assertEqual(y_labels[0], "Italy")
        self.assertEqual(len(ys), len(y_labels))

    def test_to_csvs(self):
        charts_, data, abscissa = excel_to_data.to_csvs(self.xlsFile, "Bottled")
        self.assertEqual(len(charts_[0]), 5)
        self.assertEqual(len(data), 10 * 19 + 1)
        self.assertTrue(len(abscissa) > 0)

    def test_opened_file(self):
        with open(self.xlsFile, "r+b") as xls_file:
            wb = excel_to_data.get_work_book_from_file(xls_file)
            data_sheet = wb.sheet_by_name("Bottled")
            self.assertEqual(data_sheet.name, "Bottled")

    def test_stream(self):
        with open(self.xlsFile, "r+b") as xls_file:
            wb = excel_to_data.get_work_book_from_stream(xls_file.read())
            data_sheet = wb.sheet_by_name("Bottled")
            self.assertEqual(data_sheet.name, "Bottled")

    def test_gaps(self):
        x, x_label, ys, y_labels = excel_to_data.extract_data(self.xlsFile, "Euro Unbottled")
        gap_index = y_labels.index("Greece")
        self.assertEqual(ys[gap_index][0], '')


if __name__ == '__main__':
    unittest.main()

"""Test for excel repository."""
from __future__ import unicode_literals

import datetime
import os
import unittest

import charts.repositories.excel_repository as repository


class TestExcelRepository(unittest.TestCase):
    def setUp(self):
        self.xls_filename = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                         "Import Germany.xls")

    def test_excel_repository(self):
        with open(self.xls_filename, 'rb') as xls_file:
            repo = repository.ExcelRepository(xls_file, "Bottled", "Mln Lt", "D")
            entity = repo.read()
        self.assertEqual(entity.name, 'Bottled')
        self.assertEqual(entity.x_label, 'Years')
        self.assertEqual(entity.y_labels[0], 'Italy')
        self.assertEqual(len(entity.ys[0]), 10)
        self.assertEqual(entity.title, "Import Germany")
        self.assertTrue(entity.xs[0], datetime.date)

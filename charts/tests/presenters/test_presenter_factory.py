"""Test for presenter factory."""

from __future__ import unicode_literals

import collections
import datetime as dt
import unittest

import charts.presenters.ui_presenter_factory as factory


class TestPresenterFactory(unittest.TestCase):
    def setUp(self):
        xs = [dt.date(x, 1, 1) for x in [2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013]]
        df = collections.OrderedDict([('Anni', xs),
                                      ('Italia', [94.6, 113.98, 125.84, 114.91, 120.6, 145.958, 164.206, 208.525]),
                                      ('Francia', [50.87, 54.19, 57.1, 57.49, 66.3, 87.152, 92.764, 102.777])])
        self.uc = {
            'x_label': 'Anni',
            'title': 'Import da Germania dal 2005',
            'y_labels': ['Italia', 'Francia'],
            'y_label': 'Market Share %',
            'columns': df,
            'name': 'Bottled',
        }

    def test_create_lines_presenter(self):
        presenter = factory.create_presenter('LINES', self.uc, False)
        # check interface
        attr = getattr(presenter, 'get_displayable_object')
        self.assertIsNotNone(attr)

    def test_create_dots_presenter(self):
        presenter = factory.create_presenter('DOTS', self.uc, True)
        # check interface
        attr = getattr(presenter, 'get_displayable_object')
        self.assertIsNotNone(attr)

    def test_create_line_dots_presenter(self):
        presenter = factory.create_presenter('LINE_DOTS', self.uc, False)
        # check interface
        attr = getattr(presenter, 'get_displayable_object')
        self.assertIsNotNone(attr)

    def test_create_line_with_legend_presenter(self):
        presenter = factory.create_presenter('LINES_LEGEND', self.uc, True)
        # check interface
        attr = getattr(presenter, 'get_displayable_object')
        self.assertIsNotNone(attr)

    def test_lines_presenter_selection_not_found(self):
        try:
            factory.create_presenter('fake', self.uc, False)
            self.assertTrue(False)
        except factory.PresenterSelectionNotFound:
            self.assertTrue(True)

"""Test presenters."""

from __future__ import unicode_literals

import collections
import datetime as dt
import unittest

from bokeh.plotting import Figure

import charts.presenters.ui_presenters as presenters
import charts.presenters.export_presenters as export_presenters


class TestPresenters(unittest.TestCase):
    """Test charts' presenters."""

    @staticmethod
    def get_response(name):
        xs = [dt.date(x, 1, 1) for x in [2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013]]
        df = collections.OrderedDict([('Anni', xs),
                                      ('Italia', [94.6, 113.98, 125.84, 114.91, 120.6, 145.958, 164.206, 208.525]),
                                      ('Francia', [50.87, 54.19, 57.1, 57.49, 66.3, 87.152, 92.764, 102.777])])
        return {'x_label': 'Anni',
                'title': 'Import da Germania dal 2005',
                'y_labels': ['Italia', 'Francia'],
                'y_label': 'Market Share %',
                'columns': df,
                'name': name,
                }

    def setUp(self):
        self.response = self.get_response("sheet1")

    def test_lines_presenter(self):
        """Test a presenter that uses Line multiple times
        and support column name display in hover tool."""
        presenter = presenters.LinesPresenter(self.response, True)
        plot = presenter.get_displayable_object()
        self.assertTrue(isinstance(plot, Figure))

    def test_dots_presenter(self):
        """Test a presenter that uses circle multiple times
        and support column name display in hover tool."""
        presenter = presenters.DotsPresenter(self.response, False)
        plot = presenter.get_displayable_object()
        self.assertTrue(isinstance(plot, Figure))

    def test_line_dots_presenter(self):
        """Test a presenter that uses circle and line multiple times
        and support column name display in hover tool."""
        presenter = presenters.LinesDotsPresenter(self.response, False)
        plot = presenter.get_displayable_object()
        self.assertTrue(isinstance(plot, Figure))

    def test_data_set_presenter(self):
        """Test for a presenter able to render tablib data set."""
        p = export_presenters.DataSetPresenter(self.response)
        ds = p.get_data_set()
        df = self.response['columns']
        self.assertEqual(df['Anni'][0].isoformat(), ds['Anni'][0])
        self.assertEqual(ds.title, self.response['name'])

    def test_data_book_presenter(self):
        """Test for data book presenter."""
        response2 = self.get_response("sheet2")
        p = export_presenters.DataBookPresenter([self.response, response2])
        db = p.get_data_book()
        self.assertEqual(db.size, 2)
        xls_str = export_presenters.data_book_to_xls_stream(db)
        self.assertTrue(len(xls_str) > 0)

    def test_response_with_dots_for_line_presenter(self):
        """When dots key is present in response dict draw also dots for a column listed."""
        response = self.get_response("with_dots")
        response['dots'] = ['Italia']
        response['y_labels'] = ['Francia']
        presenter = presenters.LinesPresenter(response, True)
        plot = presenter.get_displayable_object()
        self.assertTrue(isinstance(plot, Figure))

    def test_response_with_dots_for_line_presenter_with_legend(self):
        """When dots key is present in response dict draw also dots for a column listed."""
        response = self.get_response("with_dots")
        response['dots'] = ['Italia']
        response['y_labels'] = ['Francia']
        presenter = presenters.LinePresenterWithLegend(response, True)
        plot = presenter.get_displayable_object()
        self.assertTrue(isinstance(plot, Figure))

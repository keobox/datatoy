"""Business logic here."""

import collections
import itertools as itt

from charts.entities.common_entity_util import transpose


def float_if_not_null(y):
    """Converts to float for bokeh only if y is not None.
    :param y: model Data.y value
    """
    if not y:
        return y
    return float(y)


class FilterableChartUseCase:
    """Use case: filter by country and/or date a entity
    and return a data frame.
    """

    def __init__(self, entity, data_tool=None):
        chart = entity
        self.data_tool = data_tool
        self.filtered_xs = list(chart.xs)
        self.filtered_y_labels = chart.y_labels
        self.filtered_ys = list(chart.ys)
        self.from_index = 0
        self.id = chart.id
        self.name = chart.name
        self.title = chart.title
        self.to_index = len(chart.xs)
        self.x_label = chart.x_label
        self.y_label = chart.y_label
        self.ys = list(chart.ys)

    def filter(self, y_labels_filter, from_, to):
        """Set filters by country and date.
        :param y_labels_filter: list of strings
        :param from_: datetime.date
        :param to: datetime.date
        """
        # filter x axis
        x_dates = [(i, x) for i, x in enumerate(self.filtered_xs) if from_ <= x <= to]
        self.from_index = x_dates[0][0]
        self.to_index = x_dates[-1][0] + 1
        xs = [x[1] for x in x_dates]
        self.filtered_xs = xs
        # filter ys axis and y_labels
        y_labels_filter_set = set(y_labels_filter)
        filtered_ys = []
        for i, ys in enumerate(self.filtered_ys):
            if self.filtered_y_labels[i] in y_labels_filter_set:
                filtered_ys.append(ys[self.from_index:self.to_index])
        self.filtered_ys = filtered_ys
        self.filtered_y_labels = y_labels_filter

    def execute(self):
        """Returns a filtered dataframe.
        :return: OrderedDict.
        """
        df = collections.OrderedDict()
        df[self.x_label] = self.filtered_xs
        for i, ys in enumerate(self.filtered_ys):
            all_data = [float_if_not_null(y) for y in ys]
            df[self.filtered_y_labels[i]] = all_data

        response = {'columns': df,
                    'name': self.name,
                    'title': self.title,
                    'x_label': self.x_label,
                    'y_label': self.y_label,
                    'y_labels': self.filtered_y_labels,
                    }
        if self.data_tool:
            return self.data_tool.execute(response)
        else:
            return response


def zero_if_empty(x):
    """Return zero if empty.
    :param x: Any
    :return: int
    """
    if x:
        return x
    else:
        return 0


def drop_initial_zeros(rows_sum, vs):
    """Remove the initial zeros from rows_sum and align vs accordingly"""
    drop_zeros = itt.dropwhile(lambda x: x[0] == 0, zip(rows_sum, vs))
    return zip(*drop_zeros)


class PercentageChartUseCase(FilterableChartUseCase):
    """Calculate the percentage of the values respect Total column."""

    def __init__(self, chart_entity, data_tool=None):
        super().__init__(chart_entity, data_tool)
        self.y_label = '{} %'.format(self.y_label)

    def execute(self):
        """Returns a filtered dataframe,
        but before calculates percentage values
        of filtered dataset on x axis.
        :return: OrderedDict.
        """
        yt = transpose(self.ys)
        yt = yt[self.from_index:self.to_index]
        # sum vector of yt
        rows_sum = [sum([zero_if_empty(y) for y in yy]) for yy in yt]
        rs, xs = drop_initial_zeros(rows_sum, self.filtered_xs)
        self.filtered_xs = xs
        new_ys = []
        for ys in self.filtered_ys:
            _, ys = drop_initial_zeros(rows_sum, ys)
            new_ys.append([zero_if_empty(y) * 100 / rs[i] for i, y in enumerate(ys)])
        self.filtered_ys = new_ys
        return super().execute()


def percentage_var(ys):
    """Calculate the percentage variation of a sequence.
    :param ys: list of floats with Nones
    :return: list of floats with Nones
    """

    def var_with_gaps(prec, cur):
        """Calculate the percentage variation value.
        :param prec: float or None
        :param cur: float or Node
        :return: float ot None
        """
        if prec and cur:
            return (cur - prec) * 100 / prec
        else:
            return None

    try:
        var = itt.imap(var_with_gaps, ys, ys[1:] + [ys[0]])
    except AttributeError:
        var = map(var_with_gaps, ys, ys[1:] + [ys[0]])
    return list(var)[:-1]


class PercentageVariationChartUseCase(FilterableChartUseCase):
    """Given chart data calculate the percentage variation."""

    def __init__(self, chart_entity, data_tool=None):
        super().__init__(chart_entity, data_tool)
        self.y_label = '{} % Variation'.format(self.y_label)

    def execute(self):
        """Returns a filtered dataframe,
        but before calculates percentage variation of ys.
        :return: OrderedDict.
        """
        self.filtered_xs = self.filtered_xs[1:]
        self.filtered_ys = [percentage_var(ys) for ys in self.filtered_ys]
        return super().execute()


class ChartSetUseCase(object):
    """Given an entity set returns data frames.
    """

    def __init__(self, chart_entity_set):
        self.chart_entity_set = chart_entity_set

    def get_data_responses(self):
        """Returns data frames from entities.
        :return: data frame set
        """
        return [FilterableChartUseCase(ge).execute() for ge in self.chart_entity_set]

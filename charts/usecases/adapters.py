"""Adapter interfaces"""

import abc


class DataLibraryAdapter(metaclass=abc.ABCMeta):
    """For chart use cases."""

    @classmethod
    def __subclasshook__(cls, subclass):
        return hasattr(subclass, 'execute') and callable(subclass.execute)

    @abc.abstractmethod
    def execute(self, response):
        """Get use case response process it and return processed response."""

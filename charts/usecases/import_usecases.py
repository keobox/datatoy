"""Import use cases for the import view. Responsible for data transformation."""

import collections
import copy
import itertools

from charts.entities.common_entity_util import transpose
from charts.entities.chart_entity import ChartDataEntity


def fill_gaps_with_null(y):
    """Return the empty cell value with the proper type to treat it as a missing value.
    :param y: cell value
    """
    if y == '':
        return None
    if isinstance(y, str) and not y.isnumeric():
        return None
    return y


def fill_gaps_with_null_for_seq(ys):
    """Deal with missing values in sequence ys
    :param ys: list of float or empty string
    :return: list of float or None
    """
    return [fill_gaps_with_null(y) for y in ys]


def remove_empty_columns(col_names, cols):
    """
    Remove not numeric columns and fix not numeric entries
    @rtype: tuple(list, list)
    """
    cols = [fill_gaps_with_null_for_seq(y) for y in cols]
    filled_cols = [any(y) for y in cols]
    if not all(filled_cols):
        col_names = [z[0] for z in zip(col_names, filled_cols) if z[1]]
        cols = [z[0] for z in zip(cols, filled_cols) if z[1]]
    return col_names, cols


class TimeSeriesTabularUsecase:
    """
    Returns a list of entity produced by a
    "read repository" during data import
    """

    def __init__(self, data_entity):
        """
        @param entity: a chart entity
        @type entity: ChartDataEntity
        """
        self.data_entity = data_entity

    def execute(self):
        """
        Returns a list of ChartDataEntity
        @return: list
        @rtype: list
        """
        ent = self.data_entity
        ent.y_labels, ent.ys = remove_empty_columns(ent.y_labels, ent.ys)
        return [self.data_entity]


class TimeSeriesGroupByUsecase(TimeSeriesTabularUsecase):
    """Group by a column uses case"""

    def __init__(self, data_entity, group_by_col, summary_cols):
        """
        @param entity: a chart entity
        @type entity: ChartDataEntity
        """
        super().__init__(data_entity)
        self.group_by_col = group_by_col
        self.summary_cols = summary_cols

    def execute(self):
        """
        Returns a list of ChartDataEntity
        @return: list
        @rtype: list
        """
        group_index = self.data_entity.y_labels.index(self.group_by_col)
        zs = copy.deepcopy(self.data_entity.ys)
        zs.append(copy.deepcopy(self.data_entity.xs))
        zs = transpose(zs)
        groups = collections.OrderedDict()
        for group_key, group_data in itertools.groupby(zs, lambda z: z[group_index]):
            if group_key in groups:
                groups[group_key].append(list(group_data)[0])
            else:
                groups[group_key] = list(group_data)
        entities = []
        for entity_name in groups:
            cols = transpose(groups[entity_name])
            xs = cols[-1]
            ys = cols[:-1]
            y_labels, ys = remove_empty_columns(self.data_entity.y_labels, ys)
            e = ChartDataEntity(self.data_entity.id, self.data_entity.title, entity_name,
                                xs, self.data_entity.x_label,
                                ys, y_labels, entity_name)
            entities.append(e)
        if self.summary_cols:
            new_entities = []
            for summary_col in self.summary_cols.strip().split():
                y_labels = [ent.name for ent in entities]
                xs = entities[0].xs
                x_label = self.data_entity.x_label
                ys = [ent.get_col(summary_col) for ent in entities]
                e = ChartDataEntity(self.data_entity.id, self.data_entity.title,
                                    summary_col, xs, x_label,
                                    ys, y_labels, summary_col)
                new_entities.append(e)
            entities.extend(new_entities)
        return entities

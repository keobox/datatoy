"""Factories for use cases."""

from __future__ import print_function

import charts.usecases.chart_usecases as usecases
import charts.usecases.import_usecases as import_usecases


class UseCaseSelectionNotFound(Exception):
    """A use case selection was not found."""


class NotSupportedUseCase(Exception):
    """The use case is not supported yet."""


def create_usecase(usecase_selection, entity, data_tool=None):
    """Factory function.
    :param usecase_selection: str
    :param entity: entities.Chart
    :return: usecases.*
    @param data_tool: Adapter for the use case
    @type data_tool: DataLibraryAdapter
    """
    if usecase_selection == 'ORIG':
        return usecases.FilterableChartUseCase(entity, data_tool)
    elif usecase_selection == 'PERCENT':
        return usecases.PercentageChartUseCase(entity, data_tool)
    elif usecase_selection == 'VAR':
        return usecases.PercentageVariationChartUseCase(entity, data_tool)
    raise UseCaseSelectionNotFound


def create_import_usecase(usecase_selection, entity, groupby_col_name, summary_col_name):
    """ Factory Function.
    @param usecase_selection: usecase enumeration
    @type usecase_selection: basestring
    @param entity: a chart entity
    @type entity: ChartDataEntity
    @param groupby_col_name: group by aggregation column
    @type groupby_col_name: str
    @return: import_usecases.*
    @rtype: import_usecases.*
    """
    if usecase_selection == 'TABULAR':
        return import_usecases.TimeSeriesTabularUsecase(entity)
    if usecase_selection == 'GROUPBY':
        return import_usecases.TimeSeriesGroupByUsecase(entity,
                                                        groupby_col_name,
                                                        summary_col_name)
    raise NotSupportedUseCase

from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import slugify


@python_2_unicode_compatible
class Chart(models.Model):
    """Models a chart's data."""
    name = models.CharField(max_length=64)
    title = models.CharField(max_length=128)
    slug_title = models.SlugField(max_length=128)
    x_label = models.CharField(max_length=64)
    y_label = models.CharField(max_length=64)

    def save(self):
        """Added to handle slug_title."""
        self.slug_title = slugify(self.title)
        super(Chart, self).save()

    def __str__(self):
        return '%s-%s' % (self.title, self.name)

    class Meta:
        verbose_name_plural = 'Charts'


@python_2_unicode_compatible
class Abscissa(models.Model):
    """Models x axis for a chart. Asserts that x point are inserted in order."""
    x = models.DateField()
    chart = models.ForeignKey(Chart, on_delete=models.CASCADE)

    def __str__(self):
        return '%s-%s-%s' % (self.chart.title, self.chart.name, self.x)

    class Meta:
        verbose_name_plural = 'Chart Abscissa'


@python_2_unicode_compatible
class Data(models.Model):
    """Models y axes of a chart. Asserts that data is inserted in order."""
    column = models.CharField(max_length=64)
    y = models.DecimalField(max_digits=16, decimal_places=4, null=True)
    chart = models.ForeignKey(Chart, on_delete=models.CASCADE)

    def __str__(self):
        if not self.y:
            y_desc = 'missing'
        else:
            y_desc = self.y
        return '%s-%s-%s-%s' % (self.chart.title, self.chart.name, self.column, y_desc)

    class Meta:
        verbose_name_plural = 'Chart Data'

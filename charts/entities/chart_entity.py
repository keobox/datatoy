"""Entities of the charts application."""


class ChartDataEntity(object):
    """Chart Data Entity."""

    def __init__(self, id_, title, name, xs, x_label, ys, y_labels, y_label):
        self.id = id_
        # Issue with 'x','title' and 'y' parameters of TimeSeries,
        # if I pass unicode strings the x scale
        # is shifted by one step.
        # convert to plain strings
        self.title = str(title)
        self.name = str(name)
        self.xs = xs
        self.x_label = str(x_label)
        self.ys = ys
        self.y_labels = y_labels
        self.y_label = str(y_label)

    def __str__(self):
        sl = ["id = %s" % self.id, "title = '%s'" % self.title,
              "name = '%s'" % self.name, "xs = %s" % self.xs,
              "x_label = '%s'" % self.x_label, "ys = %s" % self.ys,
              "y_labels = %s" % self.y_labels, "y_unit_name = '%s'" % self.y_label]
        return "\n".join(sl)

    def get_col(self, col_name):
        """
        Get the column content of col_name
        @param col_name: column name
        @type col_name: str
        @return: list
        @rtype: list
        """
        index = self.y_labels.index(col_name)
        return self.ys[index]

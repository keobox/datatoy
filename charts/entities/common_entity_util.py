"""Some common utilities for entity handling."""


def transpose(ys):
    """
    Transpose ys.
    :param: ys list of list
    """
    return [list(y) for y in zip(*ys)]

from django.contrib import admin

from .models import Abscissa
from .models import Data
from .models import Chart


class ChartAdminOptions(admin.ModelAdmin):
    """Admin customization for chart."""
    search_fields = ['title', 'name']
    prepopulated_fields = {'slug_title': ('title',)}


class AbscissaAdminOptions(admin.ModelAdmin):
    """Admin customization for abscissa."""


class DataAdminOptions(admin.ModelAdmin):
    """Admin customization for data."""
    search_fields = ['column']


admin.site.register(Abscissa, AbscissaAdminOptions)
admin.site.register(Data, DataAdminOptions)
admin.site.register(Chart, ChartAdminOptions)

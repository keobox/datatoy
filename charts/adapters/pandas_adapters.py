"""Use case adapters for pandas API."""

import copy
import datetime

import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from scipy.optimize import fsolve
from charts.usecases.adapters import DataLibraryAdapter


class SimpleMovingAverage(DataLibraryAdapter):
    """Moving average of all the columns in response."""

    def __init__(self, window_size):
        self.window_size = window_size

    def execute(self, response):
        data = copy.deepcopy(response)
        df = pd.DataFrame.from_dict(data['columns'])
        for col in data['y_labels']:
            df[col] = df[col].rolling(self.window_size, min_periods=1).mean()
        data['columns'] = df.to_dict()
        return data


def logistic_model(x, a, b, c):
    return c / (1 + np.exp(-(x - b) / a))


class SingleVariableLogisticRegression(DataLibraryAdapter):
    """Add the logistic regression of a single column."""

    def __init__(self, window_size):
        self.window_size = window_size
        self.a = 0
        self.b = 0
        self.c = 0
        self.data = None
        self.xn = []
        self.xs = []
        self.y0 = 0
        self.ys = []

    def process_response(self):
        """After fitting process response."""
        # add samples to x and calculate logistic function
        last_x = self.xn[-1]
        self.xn = self.xn + list(range(last_x + 1, last_x + self.window_size + 1))
        y_logistic = [logistic_model(x, self.a, self.b, self.c) for x in self.xn]
        # add y0 back
        y_logistic = [y + self.y0 for y in y_logistic]
        # add nulls to ys
        self.ys = self.ys + [None] * self.window_size
        assert len(y_logistic) == len(self.ys)
        # add window_size days to xs
        last_day = self.xs[-1]
        days = [last_day + datetime.timedelta(days=d) for d in range(1, self.window_size + 1)]
        self.xs = self.xs + days
        assert len(self.xs) == len(y_logistic)
        # prepare data
        self.data['columns'][self.data['x_label']] = self.xs
        self.data['columns'][self.data['y_labels'][0]] = self.ys
        self.data['y_labels'].append(self.data['y_labels'][0] + '_logistic')
        self.data['columns'][self.data['y_labels'][1]] = y_logistic

    def execute(self, response):
        if len(response['columns']) > 2:
            raise NotImplementedError("Multiple columns are not supported")
        self.data = response
        # assuming that initial date it's the beginning of the event
        # numeric index, assuming index are days
        self.xs = self.data['columns'][self.data['x_label']]
        x0 = self.xs[0]
        self.xn = [(x - x0).days for x in self.xs]
        self.ys = self.data['columns'][self.data['y_labels'][0]]
        self.y0 = self.ys[0]
        ys0 = [y - self.y0 for y in self.ys]
        # fitting
        fit = curve_fit(logistic_model, self.xn, ys0)
        self.a, self.b, self.c = fit[0]
        self.process_response()
        return self.data


class Day0Predictor(SingleVariableLogisticRegression):
    """Solves Logistic Curve = c equation."""

    def process_response(self):
        day_0 = int(fsolve(lambda x: logistic_model(x, self.a, self.b, self.c) - int(self.c), self.b)[0])
        days_since_beginning = len(self.xs)
        self.window_size = day_0 - days_since_beginning
        super().process_response()
        self.data['dots'] = [self.data['y_labels'][0] + '_day0']
        # due rounding c is below the asymptote,
        # so taking the last value of logistic curve
        # cs = [None] * (day_0 - 1) + [self.c]
        y_last = self.data['columns'][self.data['y_labels'][0] + '_logistic'][-1]
        cs = [None] * (day_0 - 1) + [y_last]
        assert len(self.xs) == len(cs)
        self.data['columns'][self.data['dots'][0]] = cs

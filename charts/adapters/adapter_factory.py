"""Adapters' factory function."""

from charts.adapters.pandas_adapters import Day0Predictor
from charts.adapters.pandas_adapters import SimpleMovingAverage
from charts.adapters.pandas_adapters import SingleVariableLogisticRegression


def create_adapter(adapter_selection, **params):
    if adapter_selection == 'SMA':
        return SimpleMovingAverage(int(params['window_size']))
    if adapter_selection == 'LR':
        return SingleVariableLogisticRegression(int(params['window_size']))
    if adapter_selection == 'DAY0':
        return Day0Predictor(int(params['window_size']))
    return None

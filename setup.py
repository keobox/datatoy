#!/usr/bin/env python

from setuptools import setup, find_packages

setup (
    name='datatoy',
    version='1.0',
    url='https://bitbucket.org/keobox/datatoy',
    author='Cesare Placanica',
    author_email='keobox@gmail.com',
    license='MIT',
    packages=find_packages(),
    description='Data visualization pet project',
    long_description='Data visualization pet project',
    classifiers=(
    ),
    install_requires=[],
    scripts=[]
)

import numpy as np
from scipy.optimize import curve_fit
from scipy.optimize import fsolve

from datatoy import wsgi
import datetime
import charts.repositories.model_repository as repo
import charts.usecases.chart_usecases as use_cases


def logistic_model(x, a, b, c):
    return c / (1 + np.exp(-(x - b) / a))


def predict_day_0(title, name, from_='', to='', show_error=False):
    r = repo.ChartRepository(0)
    chart = r.find_by_title_and_name(title, name)
    print(chart.title, chart.name)
    from_date = chart.xs[0]
    to_date = chart.xs[-1]
    if from_:
        from_date = datetime.datetime.strptime(from_, '%Y-%m-%d').date()
    if to:
        to_date = datetime.datetime.strptime(to, '%Y-%m-%d').date()
    uc = use_cases.FilterableChartUseCase(chart)
    uc.filter(['totale_casi'], from_date, to_date)
    # xs = [(x - datetime.date(2020, 1, 1)).days for x in uc.filtered_xs]
    xs = [(x - from_date).days for x in uc.filtered_xs]
    ys = uc.filtered_ys[0]
    y0 = ys[0]
    ys0 = [y - y0 for y in ys]
    # guesses = [5, 100, float(ys[-1])]
    # fit = curve_fit(logistic_model, xs, ys, p0=guesses)
    # fit = curve_fit(logistic_model, xs, ys0, p0=guesses)
    fit = curve_fit(logistic_model, xs, ys0)
    a, b, c = fit[0]
    if show_error:
        errors = [np.sqrt(fit[1][i][i]) for i in [0, 1, 2]]
        ea, eb, ec = errors
        print('a {} error {}'.format(a, ea))
        print('b {} error {}'.format(b, eb))
        print('c {} error {}'.format(c, ec))
    print('Data set From: {} To: {}'.format(from_date, to_date))
    print('New infections: ', ys[-1] - ys[-2])
    day_0 = int(fsolve(lambda x: logistic_model(x, a, b, c) - int(c), b)[0])
    # print('Day 0 new infections: ', day_0, datetime.date(2020, 1, 1) + datetime.timedelta(days=day_0))
    print('Day 0 new infections: ', day_0, from_date + datetime.timedelta(days=day_0))
    print()


if __name__ == '__main__':
    print('First event')
    predict_day_0('dpc-covid19-ita-andamento-nazionale', 'Andamento Nazionale', to='2020-07-20')
    predict_day_0('dpc-covid19-ita-province', 'Milano', to='2020-07-20')
    predict_day_0('dpc-covid19-ita-regioni', 'Lombardia', to='2020-07-20')
    print('Second event')
    predict_day_0('dpc-covid19-ita-andamento-nazionale', 'Andamento Nazionale', from_='2020-08-15')
    predict_day_0('dpc-covid19-ita-province', 'Milano', from_='2020-08-01')
    predict_day_0('dpc-covid19-ita-regioni', 'Lombardia', from_='2020-08-01')

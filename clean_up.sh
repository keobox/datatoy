#!/bin/sh

rm -rf .tox/
rm -rf .cache/
find . -name "*.pyc" -exec rm -f {} \;
